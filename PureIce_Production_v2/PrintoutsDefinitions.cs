//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PureIce_Production_v2
{
    using System;
    using System.Collections.Generic;
    
    public partial class PrintoutsDefinitions
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PrintoutsDefinitions()
        {
            this.Printouts = new HashSet<Printouts>();
            this.PrintoutsDatasources = new HashSet<PrintoutsDatasources>();
        }
    
        public int Id { get; set; }
        public string DefinitionName { get; set; }
        public System.DateTime CreateDate { get; set; }
        public System.DateTime LastChange { get; set; }
        public bool IsCorrect { get; set; }
        public int DefinitionClass { get; set; }
        public string Layout { get; set; }
        public byte[] DefinitionHash { get; set; }
        public string Styles { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Printouts> Printouts { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PrintoutsDatasources> PrintoutsDatasources { get; set; }
    }
}
