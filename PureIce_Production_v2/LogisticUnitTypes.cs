//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PureIce_Production_v2
{
    using System;
    using System.Collections.Generic;
    
    public partial class LogisticUnitTypes
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LogisticUnitTypes()
        {
            this.AllowedLogisticUnits = new HashSet<AllowedLogisticUnits>();
            this.LogisticUnitObjects = new HashSet<LogisticUnitObjects>();
            this.LogisticUnitsLimits = new HashSet<LogisticUnitsLimits>();
            this.UsedBarcodes = new HashSet<UsedBarcodes>();
            this.Norms = new HashSet<Norms>();
            this.OperatorsDocumentsBarcodes = new HashSet<OperatorsDocumentsBarcodes>();
            this.Printouts = new HashSet<Printouts>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Width { get; set; }
        public decimal Length { get; set; }
        public decimal Height { get; set; }
        public decimal OwnWeight { get; set; }
        public bool IsActive { get; set; }
        public bool IsHomogeneous { get; set; }
        public decimal MaxHeight { get; set; }
        public decimal MaxWeight { get; set; }
        public string InternalName { get; set; }
        public int BarcodeTypeId { get; set; }
        public bool IsDefault { get; set; }
        public string BarcodeRangeMinimum { get; set; }
        public string BarcodeRangeMaximum { get; set; }
        public int DefaultNumberOfGeneratedBarcodes { get; set; }
        public Nullable<int> ErpUnitId { get; set; }
        public string SSCC_ExtensionDigitExpression { get; set; }
        public string SSCC_CompanyPrefix { get; set; }
        public Nullable<int> SSCC_SerialReferenceLength { get; set; }
        public Nullable<int> SSCC_SerialReferencePrefixLength { get; set; }
        public string SSCC_SerialReferencePrefixExpression { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AllowedLogisticUnits> AllowedLogisticUnits { get; set; }
        public virtual LogisticUnitBarcodeTypes LogisticUnitBarcodeTypes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LogisticUnitObjects> LogisticUnitObjects { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LogisticUnitsLimits> LogisticUnitsLimits { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UsedBarcodes> UsedBarcodes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Norms> Norms { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OperatorsDocumentsBarcodes> OperatorsDocumentsBarcodes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Printouts> Printouts { get; set; }
    }
}
