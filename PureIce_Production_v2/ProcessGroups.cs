//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PureIce_Production_v2
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProcessGroups
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProcessGroups()
        {
            this.Processes = new HashSet<Processes>();
            this.ProcessGroups1 = new HashSet<ProcessGroups>();
            this.ProcessStages = new HashSet<ProcessStages>();
            this.ProcessTasks = new HashSet<ProcessTasks>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDocument { get; set; }
        public bool IsPosition { get; set; }
        public bool IsList { get; set; }
        public bool IsMobile { get; set; }
        public bool IsAcceptance { get; set; }
        public bool IsRelease { get; set; }
        public bool IsMovement { get; set; }
        public bool IsOrder { get; set; }
        public Nullable<int> DocumentSpecies { get; set; }
        public bool IsCancellingDocument { get; set; }
        public Nullable<int> PositionProcessType { get; set; }
        public bool IsErp { get; set; }
        public bool IsMultiposition { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Processes> Processes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProcessGroups> ProcessGroups1 { get; set; }
        public virtual ProcessGroups ProcessGroups2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProcessStages> ProcessStages { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProcessTasks> ProcessTasks { get; set; }
    }
}
