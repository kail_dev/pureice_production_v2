//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PureIce_Production_v2
{
    using System;
    using System.Collections.Generic;
    
    public partial class ZZSPE_FROM_PLC
    {
        public int ID { get; set; }
        public Nullable<int> MSG_ID { get; set; }
        public Nullable<int> PROG_MSG { get; set; }
        public Nullable<int> ROK { get; set; }
        public Nullable<int> MIESIAC { get; set; }
        public Nullable<int> DZIEN { get; set; }
        public Nullable<int> GODZINA { get; set; }
        public Nullable<int> MINUTA { get; set; }
        public Nullable<int> SEKUNDA { get; set; }
        public Nullable<int> ZLEC_LINE { get; set; }
        public Nullable<int> TWR_GID { get; set; }
        public Nullable<int> ZLEC_GID { get; set; }
        public Nullable<int> ILOSC { get; set; }
        public Nullable<int> PRINTER_ID { get; set; }
        public Nullable<int> STAN { get; set; }
        public Nullable<int> ZMIANA { get; set; }
        public Nullable<int> NR_PALETY { get; set; }
        public Nullable<System.DateTime> DATA_DODANIA { get; set; }
        public string PW_DOKUMENT { get; set; }
        public string API_STATUS { get; set; }
    }
}
