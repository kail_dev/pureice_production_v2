//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PureIce_Production_v2
{
    using System;
    using System.Collections.Generic;
    
    public partial class ShippingOrderTemplates
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ShippingOrderTemplates()
        {
            this.ShippingConfiguration = new HashSet<ShippingConfiguration>();
        }
    
        public int Id { get; set; }
        public string InternalName { get; set; }
        public Nullable<int> NameTranslationId { get; set; }
        public Nullable<int> ShipperId { get; set; }
    
        public virtual Shippers Shippers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ShippingConfiguration> ShippingConfiguration { get; set; }
        public virtual Translations Translations { get; set; }
    }
}
