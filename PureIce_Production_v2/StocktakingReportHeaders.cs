//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PureIce_Production_v2
{
    using System;
    using System.Collections.Generic;
    
    public partial class StocktakingReportHeaders
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public StocktakingReportHeaders()
        {
            this.StocktakingReportDocuments = new HashSet<StocktakingReportDocuments>();
            this.StocktakingReportQueueMessages = new HashSet<StocktakingReportQueueMessages>();
            this.StocktakingReportStatuses = new HashSet<StocktakingReportStatuses>();
            this.StocktakingReportUnconfirmedDocuments = new HashSet<StocktakingReportUnconfirmedDocuments>();
        }
    
        public int Id { get; set; }
        public int StocktakingSheetId { get; set; }
        public Nullable<int> QueueMessageId { get; set; }
        public byte ReportType { get; set; }
        public System.DateTime CreationStartDate { get; set; }
        public Nullable<System.DateTime> CreationEndDate { get; set; }
    
        public virtual Queue Queue { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StocktakingReportDocuments> StocktakingReportDocuments { get; set; }
        public virtual StocktakingSheets StocktakingSheets { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StocktakingReportQueueMessages> StocktakingReportQueueMessages { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StocktakingReportStatuses> StocktakingReportStatuses { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StocktakingReportUnconfirmedDocuments> StocktakingReportUnconfirmedDocuments { get; set; }
    }
}
