﻿using PureIce_Production_v2.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace PureIce_Production_v2
{
    /// <summary>
    /// Logika interakcji dla klasy add.xaml
    /// </summary>
    public partial class add : Window
    {
        private int NRID;

        private int idoper { get; set; }
        private int Msg { get; set; }
        private int ProgMsg { get; set; }

        private int MSG_ID;
        private int? PROG_MSG;
        private int? ROK;
        private int? MIESIAC;
        private int? DZIEN;
        private int? GODZINA;
        private int? MINUTA;
        private int? SEKUNDA;
        private int? ZLEC_LINE;
        private int? TWR_GID;
        private int? ZLEC_GID;
        private int? ILOSC;
        private int? PRINTER_ID;
        private int STAN;
        private int? ZMIANA;
        private int? NR_PALETY;
        private string EAN;
        private string EANOPAK;
        private int DOK_GIDTyp;

        private string Twrkod { get; set; }
       
        private string paleta { get; set; }
        /*public add(int idoperacji, string twrkod, int miesiac, int dzien, int twrgid, int zlecgid )
        {
            
            Id = idoperacji;
            Twrkod = twrkod;
            Miesiac = miesiac;
            Dzien = dzien;
            Twrgid = twrgid;
            Zlecgid = zlecgid; */
            public add(ZZSPE_FROM_PLC e)
            {
            dataload _dataload = new dataload();
            NRID = e.ID;
     idoper = _dataload.idczynnoscitoclick(e.ZLEC_GID.Value);
            MSG_ID = e.MSG_ID.Value;
            PROG_MSG = e.PROG_MSG;
            ROK = e.ROK;
            MIESIAC = e.MIESIAC;
            DZIEN = e.DZIEN;
            GODZINA = e.GODZINA;
            MINUTA = e.MINUTA;
            SEKUNDA = e.SEKUNDA;
            ZLEC_LINE = e.ZLEC_LINE;
            TWR_GID = e.TWR_GID;
            ZLEC_GID =e.ZLEC_GID;
            ILOSC = e.ILOSC;
            PRINTER_ID = e.PRINTER_ID;
            STAN = 3;
            ZMIANA = e.ZMIANA;
            NR_PALETY = e.NR_PALETY;
           
            EAN = _dataload.TwrEan(e.TWR_GID.Value);
            EANOPAK = _dataload.EanOpak(e.TWR_GID.Value);
          
         //   KOD_ZBIORCZY = EAN + _dataload.EANPALETY(e.TWR_GID.Value) + Cecha;
            DOK_GIDTyp = 1617;
            Twrkod = _dataload.IdToTwrKod(e.TWR_GID.Value);

            //      Id = idoperacji;
            //    Twrkod = twrkod;
            //  Miesiac = miesiac;
            // Dzien = dzien;
            // Twrgid = twrgid;
            //Zlecgid = zlecgid;

            InitializeComponent();
        
        }

        private void Realizuj_Click(object sender, RoutedEventArgs e)
        {
            string nr = "";
       //    Nr_palety.Text

        }

        private void Window_Closed (object sender, EventArgs e ) 
        {
            bool? dialogResult = null;
            RadWindow.Confirm(new DialogParameters
            {
                Header = "Czy zrealizować pozycje?",
                OkButtonContent = "Tak",
                CancelButtonContent = "Nie, tylko zamknij okno",
                Content = "Czy zrealizować pozycje?",
                Closed = (confirmDialog, eventArgs) =>
                {
                    dialogResult = eventArgs.DialogResult;
                }

            });
            if (dialogResult == true)
            {
                if(Pal.Text.Length==0)
                {
                    MessageBox.Show("Błąd! Nie podano numeru palety!");
              
                    }
                else
                    {

                    xl _xl = new xl();
                    var list = _xl.Prodrealization(idoper, Twrkod, DZIEN.Value, MIESIAC.Value, TWR_GID.Value, ZLEC_GID.Value,NRID,1);
                    //    string ss = Nr_palety.
                    //   Pal.Text.
                    ZZSPE_PRINT x = new ZZSPE_PRINT();

                    ZZSPE_DaneWeHist w = new ZZSPE_DaneWeHist();
                    dataload _dataload = new dataload();
                    if (list[0].Length > 0)
                    {

                        Entities1 en = new Entities1();
                        w.TWR_GID = TWR_GID;
                        var datan = en.Datetime(ROK, MIESIAC, DZIEN, GODZINA, MINUTA, w.TWR_GID, SEKUNDA).SingleOrDefault();
                        DateTime dd = Convert.ToDateTime(datan);

                        string data = dd.ToString("MM.yyyy");
                        string data2 = dd.ToString("yyMM")+"00";
                        string eanpal = _dataload.EANPALETY(w.TWR_GID.Value);
                        w.MSG_ID = MSG_ID;
                        w.PROG_MSG = PROG_MSG;
                        w.ROK = ROK;
                        w.ZLEC_LINE = ZLEC_LINE;
                        w.DZIEN = DZIEN;
                        w.MIESIAC = MIESIAC;
                        w.GODZINA = GODZINA;
                        w.MINUTA = MINUTA.Value;
                        w.SEKUNDA = SEKUNDA;
                        w.PRINTER_ID = 2;
                        w.SSCC = list[0];
                        w.EAN = EAN;
                        w.EANOPAK = EANOPAK;
                        w.Cecha = list[1];

                        w.KOD_ZBIORCZY = w.EAN + eanpal + w.Cecha;
                        w.ILOSC = ILOSC;
                        w.DST_GIDTyp = 160;
                        w.DOK_GIDTyp = 1617;
                        w.ZLEC_GID = ZLEC_GID;
                        w.ZLEC_LINE = ZLEC_LINE;
                        w.DOK_GIDNumer = Convert.ToInt32(list[3]);
                        w.DST_GIDNumer = Convert.ToInt32(list[2]);
                        w.ZMIANA = ZMIANA;
                        string nrp = Pal.Text.ToString();
                        w.STAN = 0;
                        w.DOK_GIDTyp = 1617;
                        w.ID = NRID;

                        w.NR_PALETY = Convert.ToInt32(nrp);

                        _dataload.SaveDWH(w);
                     //   Entities1 en = new Entities1();
                         en.Datetime(ROK, MIESIAC, DZIEN, GODZINA, MINUTA, w.TWR_GID, SEKUNDA).SingleOrDefault();
                        int dokgid = Convert.ToInt32(list[3]);
                        string dok = en.ZZSPE_DOKUMENT(1617, dokgid).FirstOrDefault();
                        string reali = list[0].ToString();
                        decimal pal = _dataload.IloscNaPalecie(w.TWR_GID.Value);
                        decimal opak = _dataload.IloscOpak(w.TWR_GID.Value);
                        //    List<string> dokument = new List<string>();
                        x.ID_PRINTER = 2;
                        x.DOK_GIDNumer = w.DOK_GIDNumer;
                        x.DOK_GIDTyp = 1617;
                        x.STATUS = 0;
                        x.ZPL_ID = 1;
                        x.TWR_WAGA= Convert.ToInt32(pal);
                        x.DOK_NAZ = dok;
                        x.TWR_SSCC = w.SSCC;
                        x.TWR_EAN_OPAK = w.EANOPAK;
                        x.TWR_EAN_PALETY = eanpal;
                        x.TWR_ILOSC = Convert.ToInt32(pal);
                        x.TWR_KOD_ZBIORCZY = w.KOD_ZBIORCZY;
                        x.TWR_CECHA = w.Cecha;
                        x.NR_PALETY = w.NR_PALETY;
                        x.TWR_NAZWA = _dataload.IdToTwrNazwa(w.TWR_GID.Value);
                        x.TWR_EAN = w.EAN;
                        x.TWR_ILOSC_OPAK = Convert.ToInt32(pal) / Convert.ToInt32(opak);
                        x.TWR_ILOSC_SZT = Convert.ToInt32(pal);
                        x.TWR_DATA = data;
                        x.TWR_DATA2 = data2;
                        _dataload.Print(x);

                        MessageBox.Show("Paleta dodana! \n Nazwa dokumentu " + dok + "\nSSCC:" + w.SSCC);
                    }
                }

            }
        }

        private void Nr_palety_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            
        }

        private void Pal_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(Pal.Text, "[^0-9]"))
            {
                MessageBox.Show("Dozwolone tylko cyfry!");
                Pal.Text = Pal.Text.Remove(Pal.Text.Length - 1);
            }
        }

        private void Dodaj_Click(object sender, RoutedEventArgs e)
        {
            if (Pal.Text.Length==0)
            {
                MessageBox.Show("Podaj numer palety!");
            }
            else
            {
                this.Close();
            }
          
        }
    }
}
