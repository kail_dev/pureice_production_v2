//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PureIce_Production_v2
{
    using System;
    using System.Collections.Generic;
    
    public partial class ObjectOperations
    {
        public int Id { get; set; }
        public string OperatorLogin { get; set; }
        public short ObjectTypeId { get; set; }
        public int ObjectId { get; set; }
        public bool ExclusiveAccess { get; set; }
        public Nullable<System.DateTime> StartTime { get; set; }
        public Nullable<System.DateTime> EndTime { get; set; }
        public string SessionAppName { get; set; }
        public Nullable<int> SessionId { get; set; }
    
        public virtual ObjectTypes ObjectTypes { get; set; }
        public virtual Sessions Sessions { get; set; }
    }
}
