﻿using NLog;
using PureIce_Production_v2.PLC;
using PureIce_Production_v2.SQL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PureIce_Production_v2
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>

   
    public partial class MainWindow : Window

    {   private static Logger logger = LogManager.GetCurrentClassLogger();


        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        private string md5hash(string file)
        {
            string check;
            using (FileStream FileCheck = File.OpenRead(file))
            {
                MD5 md5 = new MD5CryptoServiceProvider();
                byte[] md5Hash = md5.ComputeHash(FileCheck);
                check = BitConverter.ToString(md5Hash).Replace("-", "").ToLower();
            }

            return check;
        }
        public MainWindowViewModel viewModel { get; set; }
        public static Process PriorProcess()
        // Returns a System.Diagnostics.Process pointing to
        // a pre-existing process with the same name as the
        // current one, if any; or null if the current process
        // is unique.
        {
            Process curr = Process.GetCurrentProcess();
            
            Process[] procs = Process.GetProcessesByName(curr.ProcessName);
            foreach (Process p in procs)
            {
                if ((p.Id != curr.Id) &&
                    (p.MainModule.FileName == curr.MainModule.FileName))
                    return p;
            }
            return null;
        }
        private static Mutex mutex = null;
        public MainWindow()
        {
            const string appName = "PureIce_Production v3";
            bool createdNew;

            

            mutex = new Mutex(true, appName, out createdNew);

            if (!createdNew)
            {
               MessageBox.Show(appName + " jest już uruchomiony! ");

                this.Close();
            }

           
        



        DateTime thisDay = DateTime.Now;
            var config = new NLog.Config.LoggingConfiguration();

            // Targets where to log to: File and Console
            var logfile = new NLog.Targets.FileTarget("logfile") { FileName = @"log\" + "log" + thisDay + ".txt" };

            var logconsole = new NLog.Targets.ConsoleTarget("logconsole");

            // Rules for mapping loggers to targets            
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logconsole);
            config.AddRule(LogLevel.Debug, LogLevel.Fatal, logfile);
            NLog.LogManager.Configuration = config;
            InitializeComponent();
            viewModel = new MainWindowViewModel();
            DataContext = viewModel;
         //   Realizuj.IsEnabled = false;
            dataload _dataload = new dataload();
            // Stop_send.IsEnabled = false;
              Stop_Dok.IsEnabled = false;

            Start.IsEnabled = false;
            Start.Content = "Uruchomiona";
            viewModel.Listener();


       

            Start_Doc.IsEnabled = false;
          //  Stop_Dok.IsEnabled = true;
           viewModel.Creator();
            Send.IsEnabled = false;
            Stop_send.IsEnabled = true;
            Send.Content = "Uruchomione wysyłanie";
            viewModel.Sender();


            int idsesja = _dataload.IdSessionToUpdate();
            if (idsesja > 0)
            {
                var updated = _dataload.UpdateSession(idsesja);
            }

            viewModel.RefreshDataTable();

     



        }

       

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            Start.IsEnabled = false;
            Start.Content = "Uruchomiona";
            viewModel.Listener();
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            
          //  Start.IsEnabled = true;
            Send.IsEnabled = true;
            Send.Content = "Wysyłaj do PLC";
            Stop_send.IsEnabled = false; 
            viewModel.CancleSend();
        }

        private void Realizuj_operacje_Click(object sender, RoutedEventArgs e)
        {

            viewModel.RefreshDataTable_ALL();

            /*     ZZSPE_FROM_PLC sp = PlcData.SelectedItem as ZZSPE_FROM_PLC;
                 ZZSPE_DaneWeHist dw = new ZZSPE_DaneWeHist();

                 if (sp != null)
                 {
                     dataload _dataload = new dataload();
                     string sps = sp.ZLEC_GID.Value.ToString();
                     int idsesja = _dataload.IdSessionToUpdate();
                     if (idsesja > 0)
                     {
                         var updated = _dataload.UpdateSession(idsesja);
                     }






                     xl _xl = new xl();
                     List<string> realization = new List<string>();
              // realization=      _xl.Prodrealization(_dataload.idczynnosci(sp.ZLEC_GID.Value), _dataload.IdToTwrKod(sp.TWR_GID.Value), sp.DZIEN.Value, sp.MIESIAC.Value, sp.TWR_GID.Value);
                     //   dw.Pozycja = sp.ID;
                     // dw.ID = sp.ID;
                 //    realization = _xl.Prodrealization(_dataload.idczynnosci(169), _dataload.IdToTwrKod(16), 2, 1, 16,169);

                     dw.MSG_ID = sp.MSG_ID;
                     dw.PROG_MSG = sp.PROG_MSG;
                     dw.ROK = sp.ROK;
                     dw.MIESIAC = sp.MIESIAC;
                     dw.DZIEN = sp.DZIEN;
                     dw.GODZINA = sp.GODZINA;
                     dw.MINUTA = sp.MINUTA;
                     dw.SEKUNDA = sp.SEKUNDA;
                     dw.ZLEC_LINE = sp.ZLEC_LINE;
                     dw.TWR_GID = sp.TWR_GID;
                     dw.ZLEC_GID = sp.ZLEC_GID;
                     dw.ILOSC = sp.ILOSC;
                     dw.PRINTER_ID = sp.PRINTER_ID;
                     dw.STAN = 3;
                     dw.ZMIANA = sp.ZMIANA;
                     dw.NR_PALETY = sp.NR_PALETY;
                     dw.SSCC = realization[0];
                     dw.EAN = _dataload.TwrEan(sp.TWR_GID.Value);
                     dw.EANOPAK = _dataload.EanOpak(sp.TWR_GID.Value);
                     dw.Cecha = realization[1];
                     dw.KOD_ZBIORCZY = dw.EAN + _dataload.EANPALETY(sp.TWR_GID.Value) + dw.Cecha+"1";
                     dw.DOK_GIDTyp = 1617;


                     dw.DOK_GIDNumer = Convert.ToInt32(realization[3]);
                     dw.DST_GIDTyp = 160;
                     string eanopakstr = dw.EANOPAK.ToString();
                     string kodzb = dw.KOD_ZBIORCZY.ToString();
                     dw.DST_GIDNumer = Convert.ToInt32(realization[2]);
                     string opisdst = ("SSCC# " +dw.SSCC+"#"+ "\nEANOPPAK#" + eanopakstr+"#"+ "\nKODZBIORCZY#" + kodzb+"#");


                     _dataload.UpdateDstOpisy(Convert.ToInt32(realization[2]), opisdst);

                     if (realization[0].Length>0)
                     {

                         _dataload.SaveDWH(dw);
                         Entities1 en = new Entities1();
                         int dokgid = Convert.ToInt32(realization[3]);
                         //    List<string> dokument = new List<string>();
                         string dok = en.ZZSPE_DOKUMENT(1617, dokgid).FirstOrDefault();
                         string reali = realization[0].ToString();
                         MessageBox.Show("Paleta dodana! \n Nazwa dokumentu " + dok+ "\nSSCC:"+ dw.SSCC);

                     }


                     //sscc dodac + cecha + print! + pomyslec nad insertem do from_plc z flaga dodany z reki!

                 }
                 else
                 {
                     MessageBox.Show("Brak zaznaczenia");
                 }

                 */
        }

        private void Realizuj_Click(object sender, RoutedEventArgs e)
        {
     /*    
            ZZSPE_FROM_PLC sp = PlcData.SelectedItem as ZZSPE_FROM_PLC;
            xl _xl = new xl();
         //     _xl.Prodrealization()
         
            dataload _dataload = new dataload();


           // var confWindow = new add(_dataload.idczynnosci(sp.ZLEC_GID.Value), _dataload.IdToTwrKod(sp.TWR_GID.Value),sp.MIESIAC.Value,sp.DZIEN.Value,sp.TWR_GID.Value,sp.ZLEC_GID.Value) { Owner = this };
            var confWindow = new add(sp) { Owner = this };
            confWindow.ShowDialog();*/
        }

        private void PlcData_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangeEventArgs e)
        {
           // Realizuj.IsEnabled = true;
        }

        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            viewModel.RefreshDataTable();
        }

        private void Send_Click(object sender, RoutedEventArgs e)
        {

            Send.IsEnabled = false;
            Stop_send.IsEnabled = true;
            Send.Content = "Uruchomione wysyłanie";
            viewModel.Sender();
        }

        private void Start_Doc_Click(object sender, RoutedEventArgs e)
        {
            Start_Doc.IsEnabled = false;
            Stop_Dok.IsEnabled = true;
            viewModel.Creator();

        }

        private void Stop_Dok_Click(object sender, RoutedEventArgs e)
        {
            Start_Doc.IsEnabled = true;
            Stop_Dok.IsEnabled = false;
            viewModel.CancleDOK();
        }
    }
}

