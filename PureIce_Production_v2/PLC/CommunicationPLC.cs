﻿using PureIce_Production_v2.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PureIce_Production_v2.PLC
{
public    class CommunicationPLC
    {
        public MainWindowViewModel viewModel { get; set; }
        public  static byte[] PlcConnect(string ip, int port)
        {
            var logger = NLog.LogManager.GetCurrentClassLogger();
            try
            {


                logger.Info($"Aplikacja odbierająca sygnaly z PLC 1.2 data kompilacji 09.02.2021");
                UdpClient client = new UdpClient(port);


                IPEndPoint ep = new IPEndPoint(IPAddress.Parse(ip), port);
                client.Connect(ep);
                byte[] buffer;



                while (true)
                {


                    buffer = client.Receive(ref ep);

                    ZZSPE_FROM_PLC e = new ZZSPE_FROM_PLC();


                    if (buffer.Length > 8)
                    {











                        OptimaDbContext ent = new OptimaDbContext();


                        e.MSG_ID = BitConverter.ToInt32(buffer, 0);

                        e.PROG_MSG = BitConverter.ToInt32(buffer, 4);
                        e.ROK = BitConverter.ToInt32(buffer, 8);
                        // e.YearH = BitConverter.ToInt16(buffer, 10);
                        e.MIESIAC = BitConverter.ToInt16(buffer, 12);
                        e.DZIEN = BitConverter.ToInt16(buffer, 16);
                        e.GODZINA = BitConverter.ToInt16(buffer, 20);
                        e.MINUTA = BitConverter.ToInt16(buffer, 24);
                        e.SEKUNDA = BitConverter.ToInt16(buffer, 28);
                        e.ZLEC_LINE = BitConverter.ToInt16(buffer, 32);
                        e.TWR_GID = BitConverter.ToInt16(buffer, 36);
                        e.ZLEC_GID = BitConverter.ToInt16(buffer, 40);
                        e.NR_PALETY = BitConverter.ToInt16(buffer, 44);
                        e.PRINTER_ID = BitConverter.ToInt16(buffer, 48);

                        e.ZMIANA = BitConverter.ToInt16(buffer, 52);
                        List<byte> bytesToSend = new List<byte>();


                        bytesToSend.AddRange(BitConverter.GetBytes(e.MSG_ID.Value));
                        bytesToSend.AddRange(BitConverter.GetBytes(e.PROG_MSG.Value));

                        client.Send(bytesToSend.ToArray(), 8);



                        dataload dal = new dataload();

                        dal.SaveTelegram(e);
                   
                        /*    int idsesja = dal.IdSessionToUpdate();
                            if (idsesja > 0)
                            {
                                var updated = dal.UpdateSession(idsesja);
                            }

                            string EANTWR = dal.TwrEan(e.TWR_GID.Value);
                            string EANOPAK = dal.EanOpak(e.TWR_GID.Value);
                            string EANPAL = dal.EANPALETY(e.TWR_GID.Value);
                            string twrkod = dal.IdToTwrKod(e.TWR_GID.Value);
                            var toreprint = dal.FrameToReprint(e.DZIEN.Value, e.ROK.Value, e.MIESIAC.Value, e.GODZINA.Value, e.MINUTA.Value, e.SEKUNDA.Value, e.ZLEC_GID.Value, e.NR_PALETY.Value);

                            if (toreprint.Count == 0)
                            {

                                Entities1 en = new Entities1();
                                int idczynnosci = dal.idczynnosci(e.ZLEC_GID.Value);


                                CommunicationPLC pl = new CommunicationPLC();
                                Thread thread = new Thread(() => pl.WorkThreadFunction(idczynnosci,twrkod,e.DZIEN.Value,e.MIESIAC.Value,e.TWR_GID.Value,e.ZLEC_GID.Value));

                                thread.Start();


                                xl _xl = new xl();
                                var realization = _xl.Prodrealization(idczynnosci, twrkod, e.DZIEN.Value, e.MIESIAC.Value, e.TWR_GID.Value, e.ZLEC_GID.Value);

                                var datan = en.Datetime(e.ROK, e.MIESIAC, e.DZIEN, e.GODZINA, e.MINUTA, e.TWR_GID, e.SEKUNDA).SingleOrDefault();
                                DateTime dd = Convert.ToDateTime(datan);

                                string data = dd.ToString("MM.yyyy");
                                string data2 = dd.ToString("yyMM") + "00";




                                decimal pal = dal.IloscNaPalecie(e.TWR_GID.Value);
                                decimal opak = dal.IloscOpak(e.TWR_GID.Value);
                                int idfromplc = dal.IDFROMPLC(e.PROG_MSG.Value, e.ZLEC_GID.Value);

                                string reali = realization[0].ToString();
                                string eanpal = dal.EANPALETY(e.TWR_GID.Value);
                                ZZSPE_PRINT k = new ZZSPE_PRINT();
                                k.TWR_NAZWA = dal.IdToTwrNazwa(e.TWR_GID.Value);
                                k.TWR_EAN = EANTWR;
                                k.TWR_SSCC = realization[0];
                                k.TWR_ILOSC = Convert.ToInt32(pal);
                                k.TWR_ILOSC_OPAK = Convert.ToInt32(pal) / Convert.ToInt32(opak);
                                k.TWR_ILOSC_SZT = Convert.ToInt32(pal);
                                k.TWR_EAN_OPAK = EANOPAK;
                                k.TWR_DATA = data;
                                k.TWR_DATA2 = data2;

                                k.DOK_GIDTyp = 1617;
                                k.DOK_GIDNumer = Convert.ToInt32(realization[3]);
                                string dok = en.ZZSPE_DOKUMENT(1617, k.DOK_GIDNumer).FirstOrDefault();
                                k.DOK_NAZ = dok;
                                k.TWR_CECHA = realization[1];
                                k.TWR_KOD_ZBIORCZY = k.TWR_SSCC + eanpal + k.TWR_CECHA + "1";
                                k.STATUS = 0;
                                k.ID_PRINTER = 2;
                                k.NR_PALETY = e.NR_PALETY.Value;
                                k.TWR_EAN_PALETY = eanpal;

                                k.ZLEC_LINE = e.ZLEC_LINE;
                                k.ZPL_ID = 1;

                                k.TWR_NAZWA_DE = "";

                                string opisdst = ("SSCC# " + k.TWR_SSCC + "#" + "\nEANOPPAK#" + EANOPAK + "#" + "\nKODZBIORCZY#" + k.TWR_KOD_ZBIORCZY + "#");

                                dal.UpdateDstOpisy(Convert.ToInt32(realization[2]), opisdst);



                                ZZSPE_DaneWeHist d = new ZZSPE_DaneWeHist();

                                d.Pozycja = idfromplc;
                                d.MSG_ID = e.MSG_ID.Value;
                                d.PROG_MSG = e.PROG_MSG.Value;
                                d.ROK = e.ROK;
                                d.MIESIAC = e.MIESIAC;
                                d.DZIEN = e.DZIEN;
                                d.GODZINA = e.GODZINA;
                                d.MINUTA = e.MINUTA;
                                d.SEKUNDA = e.SEKUNDA;
                                d.ZLEC_LINE = e.ZLEC_LINE;
                                d.TWR_GID = e.TWR_GID;
                                d.ZLEC_GID = e.ZLEC_GID;
                                d.ILOSC = 1;
                                d.PRINTER_ID = e.PRINTER_ID;
                                d.STAN = 1;
                                d.ZMIANA = e.ZMIANA;
                                d.NR_PALETY = e.NR_PALETY;
                                d.SSCC = k.TWR_SSCC;
                                d.EAN = EANTWR;
                                d.EANOPAK = EANOPAK;
                                d.Cecha = k.TWR_CECHA;
                                d.KOD_ZBIORCZY = k.TWR_KOD_ZBIORCZY;
                                d.DOK_GIDNumer = k.DOK_GIDNumer;
                                d.DOK_GIDTyp = 1617;
                                d.DST_GIDNumer = Convert.ToInt32(realization[2]);
                                d.DST_GIDTyp = 160;
                                dal.SaveDWH(d);
                                logger.Info("Dodałem dokument: {0}. SSCC: {1} , CECHA: {2}", dok, d.SSCC, d.Cecha);




                            }

                            else
                            {
                                //  reprint

                                foreach (var item in toreprint)
                                {
                                    Entities1 en = new Entities1();
                                    var datan = en.Datetime(e.ROK, e.MIESIAC, e.DZIEN, e.GODZINA, e.MINUTA, e.TWR_GID, e.SEKUNDA).SingleOrDefault();
                                    DateTime dd = Convert.ToDateTime(datan);
                                    var zp = en.NazwaDokumentu(14343, e.ZLEC_GID.Value);
                                    logger.Info("Reprint dla palety nr {0} , Zlecenie : {1}", e.NR_PALETY, zp);
                                    string data = dd.ToString("MM.yyyy");
                                    string data2 = dd.ToString("yyMM") + "00";
                                    string eanpal = dal.EANPALETY(e.TWR_GID.Value);
                                    decimal pal = dal.IloscNaPalecie(e.TWR_GID.Value);
                                    decimal opak = dal.IloscOpak(e.TWR_GID.Value);
                                    ZZSPE_PRINT k = new ZZSPE_PRINT();
                                    k.TWR_NAZWA = dal.IdToTwrNazwa(e.TWR_GID.Value);
                                    k.TWR_EAN = item.EAN;
                                    k.TWR_SSCC = item.SSCC;
                                    k.TWR_ILOSC = Convert.ToInt32(pal);
                                    k.TWR_ILOSC_OPAK = Convert.ToInt32(pal) / Convert.ToInt32(opak);
                                    k.TWR_ILOSC_SZT = Convert.ToInt32(pal);
                                    k.TWR_EAN_OPAK = EANOPAK;
                                    k.TWR_DATA = data;
                                    k.TWR_DATA2 = data2;
                                    // k.DOK_NAZ = "PW";
                                    k.DOK_GIDTyp = 1617;
                                    k.DOK_GIDNumer = item.DOK_GIDNumer;
                                    string dok = en.ZZSPE_DOKUMENT(1617, k.DOK_GIDNumer).FirstOrDefault();
                                    k.DOK_NAZ = dok;
                                    k.TWR_CECHA = item.Cecha;
                                    k.TWR_KOD_ZBIORCZY = k.TWR_SSCC + eanpal + k.TWR_CECHA + "1";
                                    k.STATUS = 0;
                                    k.ID_PRINTER = 2;
                                    k.NR_PALETY = e.NR_PALETY.Value;
                                    k.TWR_EAN_PALETY = eanpal;

                                    k.ZLEC_LINE = e.ZLEC_LINE;
                                    k.ZPL_ID = 1;
                                    //    k.TWR_DATA_DE=
                                    //    k.TWR_DATA_DE2
                                    //      k.TWR_WAGA=
                                    k.TWR_NAZWA_DE = "";
                                    dal.Print(k);

                                }
                                /*

                                                        if (e.PRINTER_ID == 0)
                                                        {
                                                            var reprint = dal.FrameToReprint(e.DZIEN.Value, e.ROK.Value, e.MIESIAC.Value, e.GODZINA.Value, e.MINUTA.Value, e.SEKUNDA.Value, e.ZLEC_GID.Value, e.NR_PALETY.Value);
                                                            if (reprint.Count == 0)
                                                            {




                                                            }
                                                            else
                                                            {

                                                                foreach(var item in reprint)
                                                                {
                                                                    Entities1 en = new Entities1();
                                                                    var datan = en.Datetime(e.ROK, e.MIESIAC, e.DZIEN, e.GODZINA, e.MINUTA, e.TWR_GID, e.SEKUNDA).SingleOrDefault();
                                                                    DateTime dd = Convert.ToDateTime(datan);
                                                                    var zp=en.NazwaDokumentu(14343, e.ZLEC_GID.Value);
                                                                    logger.Info("Reprint dla palety nr {0} , Zlecenie : {1}", e.NR_PALETY, zp);
                                                                    string data = dd.ToString("MM.yyyy");
                                                                    string data2 = dd.ToString("yyMM") + "00";
                                                                    string eanpal = dal.EANPALETY(e.TWR_GID.Value);
                                                                    decimal pal = dal.IloscNaPalecie(e.TWR_GID.Value);
                                                                decimal opak = dal.IloscOpak(e.TWR_GID.Value);
                                                                    ZZSPE_PRINT k = new ZZSPE_PRINT();
                                                                 k.TWR_NAZWA = dal.IdToTwrNazwa(e.TWR_GID.Value);
                                                                    k.TWR_EAN = item.EAN;
                                                                    k.TWR_SSCC = item.SSCC;
                                                                    k.TWR_ILOSC = Convert.ToInt32(pal);
                                                                    k.TWR_ILOSC_OPAK = Convert.ToInt32(pal) / Convert.ToInt32(opak);
                                                                    k.TWR_ILOSC_SZT = Convert.ToInt32(pal);
                                                                    k.TWR_EAN_OPAK = EANOPAK;
                                                                    k.TWR_DATA = data;
                                                                    k.TWR_DATA2 = data2;
                                                                    // k.DOK_NAZ = "PW";
                                                                    k.DOK_GIDTyp = 1617;
                                                                    k.DOK_GIDNumer = item.DOK_GIDNumer;
                                                                    string dok = en.ZZSPE_DOKUMENT(1617, k.DOK_GIDNumer).FirstOrDefault();
                                                                    k.DOK_NAZ = dok;
                                                                    k.TWR_CECHA = item.Cecha;
                                                                    k.TWR_KOD_ZBIORCZY = k.TWR_SSCC + eanpal + k.TWR_CECHA + "1";
                                                                    k.STATUS = 0;
                                                                    k.ID_PRINTER = 2;
                                                                    k.NR_PALETY = e.NR_PALETY.Value;
                                                                    k.TWR_EAN_PALETY = eanpal;

                                                                    k.ZLEC_LINE = e.ZLEC_LINE;
                                                                    k.ZPL_ID = 1;
                                                                    //    k.TWR_DATA_DE=
                                                                    //    k.TWR_DATA_DE2
                                                                    //      k.TWR_WAGA=
                                                                    k.TWR_NAZWA_DE = "";
                                                                    dal.Print(k);

                                                                }

                                                            }

                                                            // dal.Print(e)
                                                        }
                                                        if (e.PRINTER_ID == 1)
                                                        {


                                                            Entities1 en = new Entities1();
                                                            int idczynnosci = dal.idczynnosci(e.ZLEC_GID.Value);
                                                            var datan = en.Datetime(e.ROK, e.MIESIAC, e.DZIEN, e.GODZINA, e.MINUTA, e.TWR_GID, e.SEKUNDA).SingleOrDefault();
                                                            DateTime dd = Convert.ToDateTime(datan);

                                                            string data = dd.ToString("MM.yyyy");
                                                            string data2 = dd.ToString("yyMM") + "00";
                                                            xl _xl = new xl();
                                                            var realization = _xl.Prodrealization(idczynnosci, twrkod, e.DZIEN.Value, e.MIESIAC.Value, e.TWR_GID.Value, e.ZLEC_GID.Value);
                                                            string kodzbiorczy = "";
                                                            decimal pal = dal.IloscNaPalecie(e.TWR_GID.Value);
                                                            decimal opak = dal.IloscOpak(e.TWR_GID.Value);
                                                            string eanpal = dal.EANPALETY(e.TWR_GID.Value);
                                                            ZZSPE_PRINT k = new ZZSPE_PRINT();
                                                            k.TWR_NAZWA = dal.IdToTwrNazwa(e.TWR_GID.Value);
                                                            k.TWR_EAN = EANTWR;
                                                            k.TWR_SSCC = realization[0];
                                                            k.TWR_ILOSC = Convert.ToInt32(pal);
                                                            k.TWR_ILOSC_OPAK = Convert.ToInt32(pal) / Convert.ToInt32(opak);
                                                            k.TWR_ILOSC_SZT = Convert.ToInt32(pal);
                                                            k.TWR_EAN_OPAK = EANOPAK;
                                                            k.TWR_DATA = data;
                                                            k.TWR_DATA2 = data2;
                                                            k.DOK_NAZ = "PW";
                                                            k.DOK_GIDTyp = 1617;
                                                            k.DOK_GIDNumer = Convert.ToInt32(realization[3]);
                                                            k.TWR_CECHA = realization[1];
                                                            k.TWR_KOD_ZBIORCZY = k.TWR_EAN + eanpal + k.TWR_CECHA;
                                                            k.STATUS = 0;
                                                            k.ID_PRINTER = 2;
                                                            k.NR_PALETY = e.NR_PALETY.Value;
                                                            k.TWR_EAN_PALETY = eanpal;

                                                            k.ZLEC_LINE = e.ZLEC_LINE;
                                                            k.ZPL_ID = 1;
                                                            //    k.TWR_DATA_DE=
                                                            //    k.TWR_DATA_DE2
                                                            //      k.TWR_WAGA=
                                                            k.TWR_NAZWA_DE = "";
                                                            //       k.TWR_DATA_DE1 =


                                                            dal.Print(k);

                                                            ZZSPE_DaneWeHist d = new ZZSPE_DaneWeHist();
                                                            d.ID = e.ID;
                                                            d.MSG_ID = e.MSG_ID.Value;
                                                            d.PROG_MSG = e.PROG_MSG.Value;
                                                            d.ROK = e.ROK;
                                                            d.MIESIAC = e.MIESIAC;
                                                            d.DZIEN = e.DZIEN;
                                                            d.GODZINA = e.GODZINA;
                                                            d.MINUTA = e.MINUTA;
                                                            d.SEKUNDA = e.SEKUNDA;
                                                            d.ZLEC_LINE = e.ZLEC_LINE;
                                                            d.TWR_GID = e.TWR_GID;
                                                            d.ZLEC_GID = e.ZLEC_GID;
                                                            d.ILOSC = e.ILOSC;
                                                            d.PRINTER_ID = e.PRINTER_ID;
                                                            d.STAN = 0;
                                                            d.ZMIANA = e.ZMIANA;
                                                            d.NR_PALETY = e.NR_PALETY;
                                                            //  d.SSCC=
                                                            d.EAN = EANTWR;
                                                            d.EANOPAK = EANOPAK;
                                                            //d.Cecha=
                                                            d.KOD_ZBIORCZY = kodzbiorczy;
                                                            d.DOK_GIDNumer = 1;
                                                            d.DOK_GIDTyp = 1617;
                                                            d.DST_GIDNumer = 1;



                                                            // dal.Print(e)
                                                        }
                                                        if (e.PRINTER_ID == 2)
                                                        {
                                                            logger.Info("Jestem w printer 2!");
                                                            int idplc = dal.IDFROMDWH(e.NR_PALETY.Value, e.ZLEC_GID.Value);
                                                            var reprint = dal.ListToReprint(idplc);
                                                            foreach (var r in reprint)
                                                            {
                                                                Entities1 en = new Entities1();
                                                                var datan = en.Datetime(r.ROK, r.MIESIAC, r.DZIEN, r.GODZINA, r.MINUTA, r.TWR_GID, r.SEKUNDA).SingleOrDefault();
                                                                DateTime dd = Convert.ToDateTime(datan);

                                                                string data = dd.ToString("MM.yyyy");
                                                                string data2 = dd.ToString("yyMM") + "00";
                                                                decimal pal = dal.IloscNaPalecie(e.TWR_GID.Value);
                                                                decimal opak = dal.IloscOpak(e.TWR_GID.Value);


                                                                string eanpal = dal.EANPALETY(e.TWR_GID.Value);
                                                                ZZSPE_PRINT k = new ZZSPE_PRINT();
                                                                k.TWR_NAZWA = dal.IdToTwrNazwa(e.TWR_GID.Value);
                                                                k.TWR_EAN = EANTWR;
                                                                k.TWR_SSCC = r.SSCC;
                                                                k.TWR_ILOSC = Convert.ToInt32(pal);
                                                                k.TWR_ILOSC_OPAK = Convert.ToInt32(pal) / Convert.ToInt32(opak);
                                                                k.TWR_ILOSC_SZT = Convert.ToInt32(pal);
                                                                k.TWR_EAN_OPAK = EANOPAK;
                                                                k.TWR_DATA = data;
                                                                k.TWR_DATA2 = data2;
                                                                // k.DOK_NAZ = "PW";
                                                                k.DOK_GIDTyp = 1617;
                                                                k.DOK_GIDNumer = r.DOK_GIDNumer;
                                                                string dok = en.ZZSPE_DOKUMENT(1617, k.DOK_GIDNumer).FirstOrDefault();
                                                                k.DOK_NAZ = dok;
                                                                k.TWR_CECHA = r.Cecha;
                                                                k.TWR_KOD_ZBIORCZY = k.TWR_EAN + eanpal + k.TWR_CECHA + "1";
                                                                k.STATUS = 0;
                                                                k.ID_PRINTER = 2;
                                                                k.NR_PALETY = e.NR_PALETY.Value;
                                                                k.TWR_EAN_PALETY = eanpal;

                                                                k.ZLEC_LINE = e.ZLEC_LINE;
                                                                k.ZPL_ID = 1;
                                                                //    k.TWR_DATA_DE=
                                                                //    k.TWR_DATA_DE2
                                                                //      k.TWR_WAGA=
                                                                k.TWR_NAZWA_DE = "";
                                                                //       k.TWR_DATA_DE1 =
                                                                string opisdst = ("SSCC# " + k.TWR_SSCC + "#" + "\nEANOPPAK#" + EANOPAK + "#" + "\nKODZBIORCZY#" + k.TWR_KOD_ZBIORCZY + "#");
                                                                dal.Print(k);
                                                            }



                                                        }


                                                            */

                        logger.Info("MSGID:{0} \nPROG_MSG:{1} \nROK:{2} \nDZIEN:{3} \nGODZINA:{4} \nMinuta:{5} \nSEKUNDA:{6} \nTWR_GID:{7} \nZLEC_GID:{8} \nNUMER PALETY:{9} \nPRINTER_ID:{10} \nZMIANA PRACOWNIKOW:{11} \nLINIA PRODUKCYJNA:{12}", e.MSG_ID, e.PROG_MSG, e.ROK, e.DZIEN, e.GODZINA, e.MINUTA, e.SEKUNDA, e.TWR_GID, e.ZLEC_GID, e.NR_PALETY, e.PRINTER_ID, e.ZMIANA, e.ZLEC_LINE);





                        
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error(e);
                throw;
            }
        
        }


        public static bool Disconnect (int port)
        {
          //  UdpClient client = new UdpClient(port);


          
      
            return true;

        }


        public void WorkThreadFunction(int idczynnosci, string twrkod, int dzen, int miesiac, int twr_gid, int zlecgid)
        {
            try
            {
                //
                xl _xl = new xl();
             //   var realization = _xl.Prodrealization(idczynnosci, twrkod,dzen, miesiac, twr_gid, zlecgid);

            }
            catch (Exception ex)
            {
                // log errors
            }
        }

    }
}
