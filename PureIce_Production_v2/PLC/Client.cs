﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace PureIce_Production_v2.PLC
{
   public class Client
    {
        public static byte[] PlcSend(string ip, int port, byte[] bytesToSend)
        {
            var logger = NLog.LogManager.GetCurrentClassLogger();
            UdpClient client = new UdpClient(port);
            IPEndPoint ep = new IPEndPoint(IPAddress.Parse(ip), port); // endpoint where server is listening (testing localy)

            client.Connect(ep);

            int res = client.Send(bytesToSend, bytesToSend.Length);

            byte[] buffer;

            buffer = client.Receive(ref ep);
            logger.Info("pierwszy bajt {0} {1} ", BitConverter.ToInt32(buffer, 0), BitConverter.ToInt32(buffer, 4));
            
            //Console.WriteLine("pierwszy bajt {0} {1} ", BitConverter.ToInt32(buffer, 0), BitConverter.ToInt32(buffer, 4));
            //    Console.WriteLine("CZAS WYSLANIA {0}", dt.ToString());
            client.Close();
            return buffer;
        }
    }
}
