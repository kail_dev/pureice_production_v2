﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using PureIce_Production_v2.SQL;
namespace PureIce_Production_v2.PLC
{
   public class SenPlcDal
    {
        public virtual void Send()
        {

            TelegramGenerator tg = new TelegramGenerator();
            dataload _dataload = new dataload();
            SenPlcDal dal = new SenPlcDal();
            while (true)
            {
                List<ZZSPE_TO_PLC> dalResult = dal.GetPlcTelegramEntity();
                
                foreach (ZZSPE_TO_PLC item in dalResult)
                {
                    byte[] telegram = tg.GeneratePlcProductsTelegram(item);
                    byte[] telLength = Client.PlcSend("192.168.0.101", 9602, telegram);
                    Console.WriteLine("lenghty: {0} {1}", telLength.Length, telegram.Length);
                    if (telLength.Length == 8)
                    {
                        _dataload.UpdatePlcTelegram(item.ID);
                    }
                }

                Thread.Sleep(1000);
            }
        }
        public virtual List<ZZSPE_TO_PLC> GetPlcTelegramEntity()
        {
            Entities1 ent = new Entities1();
            List<ZZSPE_TO_PLC> outcome = new List<ZZSPE_TO_PLC>();
            List<ZZSPE_TO_PLC> result = ent.ZZSPE_TO_PLC.Where(e => e.STAN == 1).ToList();

            foreach (ZZSPE_TO_PLC item in result)
            {
                ZZSPE_TO_PLC tpte = new ZZSPE_TO_PLC();

                tpte.ID = item.ID;
                tpte.MSG_ID = item.MSG_ID.Value;
                tpte.PROG_MSG = item.PROG_MSG.Value;
                tpte.TWR_GID = item.TWR_GID.Value;
                tpte.ZLEC_LINE_NUMBER = item.ZLEC_LINE_NUMBER.Value;
                tpte.ZLEC_GID = item.ZLEC_GID.Value;
                tpte.ZLEC_ILOSC = item.ZLEC_ILOSC.Value;

                tpte.ZLEC_NAZWA = item.ZLEC_NAZWA;
                tpte.TWR_NAZWA = item.TWR_NAZWA;
                outcome.Add(tpte);
            }

            return outcome;
        }


    }



}
