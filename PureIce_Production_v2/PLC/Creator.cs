﻿using PureIce_Production_v2.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PureIce_Production_v2.PLC
{
    public class Creator
    {

        public void creating()
        {
            var up = new object();
            string EANTWR = "";
            string EANOPAK = "";
            string EANPAL = "";
            string twrkod = "";


            dataload _dataload = new dataload();
            var logger = NLog.LogManager.GetCurrentClassLogger();
            int idsesja = _dataload.IdSessionToUpdate();
            if (idsesja > 0)
            {
                var updated = _dataload.UpdateSession(idsesja);
            }

            var frame = _dataload.FrameToWork();
            int POZYCJA = 0;

            foreach (var fram in frame)
            {
                try
                {
                    up = _dataload.StanFromPlc(fram.ID, 1);
                     EANTWR = _dataload.TwrEan(fram.TWR_GID.Value);
                     EANOPAK = _dataload.EanOpak(fram.TWR_GID.Value);
                     EANPAL = _dataload.EANPALETY(fram.TWR_GID.Value);
                    twrkod = _dataload.IdToTwrKod(fram.TWR_GID.Value);
                    POZYCJA = fram.ID;
                }

                catch (Exception ee)
                {
                    logger.Error(ee);
                    EANOPAK = 0.ToString();
                }




                var toreprint = _dataload.FrameToReprint(fram.DZIEN.Value, fram.ROK.Value, fram.MIESIAC.Value, fram.GODZINA.Value, fram.MINUTA.Value, fram.SEKUNDA.Value, fram.ZLEC_GID.Value, fram.NR_PALETY.Value);





                if (toreprint.Count == 0)
                {
                    try
                    {
                       

                        Entities1 en = new Entities1();
                        int idczynnosci = _dataload.idczynnosci(fram.ZLEC_GID.Value);




                        xl _xl = new xl();

                       
                      


                        var realization = _xl.Prodrealization(idczynnosci, twrkod, fram.DZIEN.Value, fram.MIESIAC.Value, fram.TWR_GID.Value, fram.ZLEC_GID.Value, fram.ID,fram.ZLEC_LINE.Value);
                        int giddd = Convert.ToInt32(realization[3]);
                        if (giddd != 0)
                        {


                            var datan = en.Datetime(fram.ROK, fram.MIESIAC, fram.DZIEN, fram.GODZINA, fram.MINUTA, fram.TWR_GID, fram.SEKUNDA).SingleOrDefault();
                            DateTime dd = Convert.ToDateTime(datan);

                            string data = dd.ToString("MM.yyyy");
                            string data2 = dd.ToString("yyMM") + "00";

                            decimal opak;


                            decimal pal = _dataload.IloscNaPalecie(fram.TWR_GID.Value);
                            try
                            {
                                opak = _dataload.IloscOpak(fram.TWR_GID.Value);
                            }
                            catch (Exception e
                            )
                            {
                                opak = 60;

                            }
                            int idfromplc = _dataload.IDFROMPLC(fram.PROG_MSG.Value, fram.ZLEC_GID.Value);

                            //  string reali = realization[0].ToString();
                            string eanpal = _dataload.EANPALETY(fram.TWR_GID.Value);
                            ZZSPE_PRINT k = new ZZSPE_PRINT();
                            k.TWR_NAZWA = _dataload.IdToTwrNazwa(fram.TWR_GID.Value);
                            k.TWR_EAN = EANTWR;
                            k.TWR_SSCC = realization[0];
                            k.TWR_ILOSC = Convert.ToInt32(pal);
                            k.TWR_ILOSC_OPAK = Convert.ToInt32(pal) / Convert.ToInt32(opak);
                            k.TWR_ILOSC_SZT = Convert.ToInt32(pal);
                            k.TWR_EAN_OPAK = EANOPAK;
                            k.TWR_DATA = data;
                            k.TWR_DATA2 = data2;

                            k.DOK_GIDTyp = 1617;
                            k.DOK_GIDNumer = Convert.ToInt32(realization[3]);
                            string dok = en.ZZSPE_DOKUMENT(1617, k.DOK_GIDNumer).FirstOrDefault();
                            k.DOK_NAZ = dok;
                            k.TWR_CECHA = realization[1];
                            k.TWR_KOD_ZBIORCZY = k.TWR_SSCC + eanpal + k.TWR_CECHA + "1";
                            k.STATUS = 0;
                            k.ID_PRINTER = fram.PRINTER_ID;
                            k.NR_PALETY = fram.NR_PALETY.Value;
                            k.TWR_EAN_PALETY = eanpal;

                            k.ZLEC_LINE = fram.ZLEC_LINE;
                            if (k.ZLEC_LINE == 4)
                            {
                                k.ZPL_ID = 4;
                            }
                            else
                            {
                                k.ZPL_ID = 1;
                            }


                            k.TWR_NAZWA_DE = "";

                            string opisdst = ("SSCC#" + k.TWR_SSCC + "#" + "\nEANOPPAK#" + EANOPAK + "#" + "\nKODZBIORCZY#" + k.TWR_KOD_ZBIORCZY + "#");
                            _dataload.Print(k);
                            _dataload.UpdateDstOpisy(Convert.ToInt32(realization[2]), opisdst);



                            ZZSPE_DaneWeHist d = new ZZSPE_DaneWeHist();

                            d.Pozycja = idfromplc;
                            d.MSG_ID = fram.MSG_ID.Value;
                            d.PROG_MSG = fram.PROG_MSG.Value;
                            d.ROK = fram.ROK;
                            d.MIESIAC = fram.MIESIAC;
                            d.DZIEN = fram.DZIEN;
                            d.GODZINA = fram.GODZINA;
                            d.MINUTA = fram.MINUTA;
                            d.SEKUNDA = fram.SEKUNDA;
                            d.ZLEC_LINE = fram.ZLEC_LINE;
                            d.TWR_GID = fram.TWR_GID;
                            d.ZLEC_GID = fram.ZLEC_GID;
                            d.ILOSC = 1;
                            d.PRINTER_ID = fram.PRINTER_ID;
                            d.STAN = 1;
                            d.ZMIANA = fram.ZMIANA;
                            d.NR_PALETY = fram.NR_PALETY;
                            d.SSCC = k.TWR_SSCC;
                            d.EAN = EANTWR;
                            d.EANOPAK = EANOPAK;
                            d.Cecha = k.TWR_CECHA;

                            d.KOD_ZBIORCZY = k.TWR_KOD_ZBIORCZY;
                            d.DOK_GIDNumer = k.DOK_GIDNumer;
                            d.DOK_GIDTyp = 1617;
                            d.DST_GIDNumer = Convert.ToInt32(realization[2]);
                            d.DST_GIDTyp = 160;

                            _dataload.SaveDWH(d);
                            logger.Info("Dodałem dokument: {0}. SSCC: {1} , CECHA: {2}", dok, d.SSCC, d.Cecha);

                            _dataload.UpdateFromPLC(d.ZLEC_GID.Value, d.NR_PALETY.Value, k.DOK_NAZ);
                            if (d.ZLEC_LINE == 4)
                            {
                                var rw = _dataload.FRAMERW(k.TWR_KOD_ZBIORCZY);
                                foreach(var item in rw)
                                {
                                    if (item.ID != null)
                                    {

                                        int idsesjaa = _dataload.IdSessionToUpdateCLOSE();
                                        if (idsesjaa > 0)
                                        {
                                            var updated = _dataload.UpdateSession(idsesjaa);
                                        }

                                        _xl.RWCLOSE1(item.ID.Value);
                                    }
                                    else
                                    {
                                        logger.Error("Nie mam jak zamknąć RW! Brak wygenerowanego dokumentu do PW nr: {0}", dok);
                                    }
                                   
                                }


                            }




                        }
                        else
                        {
                            logger.Error("Brak wygenerowanego PW dla ramki PLC {0}", fram.ID);
                        }



                    }
                    catch(Exception e)
                    {
                        logger.Error(e);
                    }
                }

                else
                {
                    //  reprint

                    foreach (var item in toreprint)
                    {
                        Entities1 en = new Entities1();
                        var datan = en.Datetime(fram.ROK, fram.MIESIAC, fram.DZIEN, fram.GODZINA, fram.MINUTA, fram.TWR_GID, fram.SEKUNDA).SingleOrDefault();
                        DateTime dd = Convert.ToDateTime(datan);
                        var zp = en.NazwaDokumentu(14343, fram.ZLEC_GID.Value);
                        logger.Info("Reprint dla palety nr {0} , Zlecenie : {1}", fram.NR_PALETY, zp);
                        string data = dd.ToString("MM.yyyy");
                        string data2 = dd.ToString("yyMM") + "00";
                        string eanpal = _dataload.EANPALETY(fram.TWR_GID.Value);
                        decimal pal = _dataload.IloscNaPalecie(fram.TWR_GID.Value);
                        decimal opak;
                        try
                        {
                            opak = _dataload.IloscOpak(fram.TWR_GID.Value);
                        }
                        catch (Exception e
                        )
                        {
                            opak = 60;

                        }






                        ZZSPE_PRINT k = new ZZSPE_PRINT();
                        k.TWR_NAZWA = _dataload.IdToTwrNazwa(fram.TWR_GID.Value);
                        k.TWR_EAN = item.EAN;
                        k.TWR_SSCC = item.SSCC;
                        k.TWR_ILOSC = Convert.ToInt32(pal);
                        k.TWR_ILOSC_OPAK = Convert.ToInt32(pal) / Convert.ToInt32(opak);
                        k.TWR_ILOSC_SZT = Convert.ToInt32(pal);
                        k.TWR_EAN_OPAK = EANOPAK;
                        k.TWR_DATA = data;
                        k.TWR_DATA2 = data2;
                        // k.DOK_NAZ = "PW";
                        k.DOK_GIDTyp = 1617;
                        k.DOK_GIDNumer = item.DOK_GIDNumer;
                        string dok = en.ZZSPE_DOKUMENT(1617, k.DOK_GIDNumer).FirstOrDefault();
                        k.DOK_NAZ = dok;
                        k.TWR_CECHA = item.Cecha;
                        k.TWR_KOD_ZBIORCZY = k.TWR_SSCC + eanpal + k.TWR_CECHA + "1";
                        k.STATUS = 0;
                        k.ID_PRINTER = 2;
                        k.NR_PALETY = fram.NR_PALETY.Value;
                        k.TWR_EAN_PALETY = eanpal;

                        k.ZLEC_LINE = fram.ZLEC_LINE;
                        k.ZPL_ID = 1;
                        //    k.TWR_DATA_DE=
                        //    k.TWR_DATA_DE2
                        //      k.TWR_WAGA=
                        k.TWR_NAZWA_DE = "";
                        _dataload.Print(k);
                        _dataload.UpdateFromPLCPRINT(POZYCJA);
                    }

                }


            }
        }
    }
}
