//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PureIce_Production_v2
{
    using System;
    using System.Collections.Generic;
    
    public partial class StructureObjectsDefinitions
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public StructureObjectsDefinitions()
        {
            this.StructureObjects = new HashSet<StructureObjects>();
            this.StructureObjectsDefinitions1 = new HashSet<StructureObjectsDefinitions>();
        }
    
        public int Id { get; set; }
        public int WarehouseId { get; set; }
        public Nullable<int> ParentDefinitionId { get; set; }
        public int StructureObjectTypeId { get; set; }
        public string Description { get; set; }
        public int LeftValue { get; set; }
        public int RightValue { get; set; }
        public Nullable<int> ParametersId { get; set; }
        public bool HasGeneratedObjects { get; set; }
        public bool IsArchived { get; set; }
        public bool HasCodeInfluence { get; set; }
        public Nullable<int> SourceObjectId { get; set; }
        public Nullable<int> ComponentWarehouseId { get; set; }
    
        public virtual Parameters Parameters { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StructureObjects> StructureObjects { get; set; }
        public virtual StructureObjectsTypes StructureObjectsTypes { get; set; }
        public virtual Warehouses Warehouses { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StructureObjectsDefinitions> StructureObjectsDefinitions1 { get; set; }
        public virtual StructureObjectsDefinitions StructureObjectsDefinitions2 { get; set; }
        public virtual Warehouses Warehouses1 { get; set; }
    }
}
