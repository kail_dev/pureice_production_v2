﻿using cdn_api;
using PureIce_Production_v2.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PureIce_Production_v2
{
  public  class xl
    {
        private string error;
        private bool starting;
        private string OpisBłęduAPI(Int32 wersja, Int32 numerFunkcji, Int32 numerBłędu)
        {
            cdn_api.XLKomunikatInfo_20220 xLKomunikatInfo_17 = new cdn_api.XLKomunikatInfo_20220();
            xLKomunikatInfo_17.Funkcja = numerFunkcji;
            xLKomunikatInfo_17.Blad = numerBłędu;
            xLKomunikatInfo_17.Wersja = wersja;
            cdn_api.cdn_api.XLOpisBledu(xLKomunikatInfo_17);
            string error = xLKomunikatInfo_17.OpisBledu;
            return error;
        }
        public void StartProd(int idope)
        {
            Int32 APIVersion = 20220;
            Int32 SessionID = -1;
            Int32 DocumentTypePM = 1603;
            Int32 DocumentHeaderID = 1;
            int nrgid = 0;

            string Flagi;

            string seria = "PROD";
            Int32 InfoDocumentHeader;
            Int32 InfoAddItem;
            Int32 InfoXLClose;
            List<string> Zwrot = new List<string>();
            try
            {
                dataload _dataload = new dataload();
                /**********************************Login**********************************/
                cdn_api.XLLoginInfo_20220 XLLogin = new cdn_api.XLLoginInfo_20220();
                XLLogin.Wersja = APIVersion;
                XLLogin.ProgramID = "API_START";
                XLLogin.Winieta = -1;
                XLLogin.OpeIdent = Properties.Settings.Default.USER_XL;
                XLLogin.OpeHaslo = Properties.Settings.Default.PASS_XL;
                XLLogin.TrybNaprawy = 1;
                XLLogin.UtworzWlasnaSesje = 1;
                XLLogin.TrybWsadowy = 0;
                XLLogin.Baza = Properties.Settings.Default.BAZA_XL;
                cdn_api.cdn_api.XLLogin(XLLogin, ref SessionID);

                /********************************Existing connection*********************************/
                cdn_api.XLPolaczenieInfo_20220 XLConnection = new cdn_api.XLPolaczenieInfo_20220();
                XLConnection.Wersja = APIVersion;
                Int32 Result = cdn_api.cdn_api.XLPolaczenie(XLConnection);
                String conn = XLConnection.ConnectString;
                cdn_api.XLProdRealizacjaInfo_20220 xLProdRealizacjastart = new cdn_api.XLProdRealizacjaInfo_20220();
                xLProdRealizacjastart.Akcja = 1;
                xLProdRealizacjastart.OperacjaId = idope;
                xLProdRealizacjastart.SposobRealizacji = 2;
                xLProdRealizacjastart.Wersja = 20220;
                xLProdRealizacjastart.Tryb = 2;

                InfoAddItem = cdn_api.cdn_api.XLDodajRealizacjeOperacjiProd(SessionID, xLProdRealizacjastart);
               
            }
            catch(Exception e)
            {

            }
            finally
            {



                if (cdn_api.cdn_api.XLLogout(SessionID) != 0)
                {
                    //  Console.WriteLine("Błąd przy wylogowaniu.");

                }
                else
                {
                    //     Console.WriteLine("Zakończono i wylogowano");
                }

            }

        }
        public List<string> ProdrealizationClick(int idoperacji, string twrkod, int dzien, int miesiac, int twrgid, int zlecgid)
        {
            Int32 APIVersion = 20220;
            Int32 SessionID = -1;
            Int32 DocumentTypePM = 1603;
            Int32 DocumentHeaderID = 1;
            int nrgid = 0;

            string Flagi;

            string seria = "PROD";
            Int32 InfoDocumentHeader;
            Int32 InfoAddItem;
            Int32 InfoXLClose;
            List<string> Zwrot = new List<string>();
            try


            {


                dataload _dataload = new dataload();
                /**********************************Login**********************************/
                cdn_api.XLLoginInfo_20220 XLLogin = new cdn_api.XLLoginInfo_20220();
                XLLogin.Wersja = APIVersion;
                XLLogin.ProgramID = "API_ODB";
                XLLogin.Winieta = -1;
                XLLogin.OpeIdent = "admin";
                XLLogin.OpeHaslo = "admPure_";
                XLLogin.TrybNaprawy = 1;
                XLLogin.UtworzWlasnaSesje = 1;
                XLLogin.TrybWsadowy = 0;
                XLLogin.Baza = "PURE-ICE 2021";
                cdn_api.cdn_api.XLLogin(XLLogin, ref SessionID);

                /********************************Existing connection*********************************/
                cdn_api.XLPolaczenieInfo_20220 XLConnection = new cdn_api.XLPolaczenieInfo_20220();
                XLConnection.Wersja = APIVersion;
                Int32 Result = cdn_api.cdn_api.XLPolaczenie(XLConnection);
                String conn = XLConnection.ConnectString;
                bool relopen = _dataload.Open(zlecgid);


                int idnew = _dataload.newpczid(idoperacji);
                cdn_api.XLProdRealizacjaInfo_20220 xLProdRealizacja1 = new cdn_api.XLProdRealizacjaInfo_20220();
                xLProdRealizacja1.Akcja = 2;
                xLProdRealizacja1.OperacjaId = idnew;
                xLProdRealizacja1.SposobRealizacji = 1;
                xLProdRealizacja1.Wersja = 20220;
                xLProdRealizacja1.Tryb = 2;
                InfoAddItem = cdn_api.cdn_api.XLDodajRealizacjeOperacjiProd(SessionID, xLProdRealizacja1);



                int ids = _dataload.newpczid(idoperacji);

                cdn_api.XLDokumentNagInfo_20220 XLDocumentHeader = new cdn_api.XLDokumentNagInfo_20220();
                XLDocumentHeader.Wersja = APIVersion;
                XLDocumentHeader.Typ = 1617;

                XLDocumentHeader.Akronim = "PURE ICE";
                //        XLDocumentHeader.MagazynZ = "";
                XLDocumentHeader.MagazynD = "M1";
                XLDocumentHeader.Seria = "PROD";

                // XLDocumentHeader.ZamNumer = 7;
                //XLDocumentHeader.ZamTyp = 14343;
                //XLDocumentHeader.ZwrNumer = ids;
                XLDocumentHeader.ZamTyp = 14343;


                //  XLDocumentHeader.ZamNumer = zlecgid;






                InfoDocumentHeader = cdn_api.cdn_api.XLNowyDokument(SessionID, ref DocumentHeaderID, XLDocumentHeader);


                if (InfoDocumentHeader > 0)
                {
                    error = OpisBłęduAPI(20220, 37, InfoDocumentHeader);
                    _dataload.UpdateApiFromPLC(24, error);
                }




                cdn_api.XLDokumentElemInfo_20220 XLAddItem = new cdn_api.XLDokumentElemInfo_20220();

                XLAddItem.ZamTyp = 14346;

                XLAddItem.ZamNumer = _dataload.pcaczynnosci(ids, twrgid);

                XLAddItem.Wersja = APIVersion;

                XLAddItem.TowarKod = twrkod;
                Entities1 db = new Entities1();

                // string ilo = 
                XLAddItem.Ilosc = _dataload.IloscNaPalecie(twrgid).ToString(); ;
                string SSCC = "";

                if (twrgid == 5)
                {




                    SSCC = db.test(4).FirstOrDefault();
                    Zwrot.Add(SSCC);/// SSCC DLA NIEMCA
                }
                else
                {
                    SSCC = db.test(1).FirstOrDefault();
                    Zwrot.Add(SSCC);// SSCC POLSKIE
                }
                //d    SSCC = db.ZZSPE_SSSC_GEN(1).ToString();
                string Cecha = db.ZZSPE_CECHA_GEN(dzien, miesiac).FirstOrDefault();
                Zwrot.Add(Cecha);


                XLAddItem.Cecha = Cecha;

                //           XLAddItem.DstNumer = ;

                //     XLAddItem.
                InfoAddItem = cdn_api.cdn_api.XLDodajPozycje(DocumentHeaderID, XLAddItem);
                Flagi = XLAddItem.Flagi.ToString();
                XLAddItem.Opis = (Cecha);
                Zwrot.Add(XLAddItem.DstNumer.ToString());









                cdn_api.XLZamkniecieDokumentuInfo_20220 XLClose = new cdn_api.XLZamkniecieDokumentuInfo_20220();

                XLClose.Wersja = APIVersion;



                XLClose.Tryb = 1;



                InfoXLClose = cdn_api.cdn_api.XLZamknijDokument(DocumentHeaderID, XLClose);

                Zwrot.Add(XLClose.GidNumer.ToString());



                _dataload.UpdateDostawy(XLAddItem.DstNumer, SSCC, _dataload.IloscNaPalecie(twrgid));
                int ss = _dataload.idczynnosci(zlecgid);



                //   _dataload.idczynnosci()




            }
            catch (Exception)
            {

            }
            /*********************************Close document***********************************/

            finally
            {



                if (cdn_api.cdn_api.XLLogout(SessionID) != 0)
                {
                    //  Console.WriteLine("Błąd przy wylogowaniu.");

                }
                else
                {
                    //     Console.WriteLine("Zakończono i wylogowano");
                }

            }

            return Zwrot;
        }

        public List<string>  Prodrealization(int idoperacji, string twrkod, int dzien , int miesiac, int twrgid , int zlecgid,int idframe,int zlecline)
        {
            starting = true;
            List<string> Zwrot = new List<string>();
            while (starting==true)
            {

        

            Int32 APIVersion = 20220;
            Int32 SessionID = -1;
            Int32 DocumentTypePM = 1603;
            Int32 DocumentHeaderID = 1;
            int nrgid = 0;

            string Flagi;

            string seria = "PROD";
            Int32 InfoDocumentHeader;
            Int32 InfoAddItem;
            Int32 InfoXLClose;

                try
                {
                    dataload _dataload = new dataload();
                    /**********************************Login**********************************/
                    cdn_api.XLLoginInfo_20220 XLLogin = new cdn_api.XLLoginInfo_20220();
                    XLLogin.Wersja = APIVersion;
                    XLLogin.ProgramID = "API_ODB";
                    XLLogin.Winieta = -1;
                    XLLogin.OpeIdent = Properties.Settings.Default.USER_XL;
                    XLLogin.OpeHaslo = Properties.Settings.Default.PASS_XL;
                    XLLogin.SerwerKlucza = Properties.Settings.Default.KLUCZ_XL;
                    XLLogin.TrybNaprawy = 1;
                    XLLogin.UtworzWlasnaSesje = 1;
                    XLLogin.TrybWsadowy = 0;
                    XLLogin.Baza = Properties.Settings.Default.BAZA_XL;
                    cdn_api.cdn_api.XLLogin(XLLogin, ref SessionID);

                    /********************************Existing connection*********************************/
                    cdn_api.XLPolaczenieInfo_20220 XLConnection = new cdn_api.XLPolaczenieInfo_20220();
                    XLConnection.Wersja = APIVersion;
                    Int32 Result = cdn_api.cdn_api.XLPolaczenie(XLConnection);
                    String conn = XLConnection.ConnectString;

                    bool relopen = _dataload.Open(zlecgid);
                    if (relopen == false)
                    {

                        int newid = _dataload.idczynnosciN(zlecgid);

                        bool todonew = _dataload.torealization(zlecgid);
                        if (todonew == true)
                        {


                            cdn_api.XLProdRealizacjaInfo_20220 xLProdRealizacjastart = new cdn_api.XLProdRealizacjaInfo_20220();
                            xLProdRealizacjastart.Akcja = 1;
                            xLProdRealizacjastart.OperacjaId = newid;
                            xLProdRealizacjastart.SposobRealizacji = 2;
                            xLProdRealizacjastart.Wersja = 20220;
                            xLProdRealizacjastart.Tryb = 2;



                            InfoAddItem = cdn_api.cdn_api.XLDodajRealizacjeOperacjiProd(SessionID, xLProdRealizacjastart);
                            idoperacji = _dataload.idczynnosci(zlecgid);

                        }
                        else
                        {
                            break;
                        }



                    }


                    int idnew = _dataload.newpczid(idoperacji);
                    cdn_api.XLProdRealizacjaInfo_20220 xLProdRealizacja1 = new cdn_api.XLProdRealizacjaInfo_20220();
                    xLProdRealizacja1.Akcja = 2;
                    xLProdRealizacja1.OperacjaId = idnew;
                    xLProdRealizacja1.SposobRealizacji = 1;
                    xLProdRealizacja1.Wersja = 20220;
                    xLProdRealizacja1.Tryb = 2;
                    InfoAddItem = cdn_api.cdn_api.XLDodajRealizacjeOperacjiProd(SessionID, xLProdRealizacja1);
                   

                    if (InfoAddItem > 0)
                    {
                        break;
                    }

                    int ids = _dataload.newpczid(idoperacji);

                    cdn_api.XLDokumentNagInfo_20220 XLDocumentHeader = new cdn_api.XLDokumentNagInfo_20220();
                    XLDocumentHeader.Wersja = 20220;
                    XLDocumentHeader.Typ = 1617;

                    XLDocumentHeader.Akronim = "PURE ICE";
                    //        XLDocumentHeader.MagazynZ = "";
                    if (zlecline == 4)
                    {
                        XLDocumentHeader.MagazynD = "SUR";
                    }
                    else
                    {
                        XLDocumentHeader.MagazynD = "M1";
                    }

                    XLDocumentHeader.Seria = "PROD";

                    // XLDocumentHeader.ZamNumer = 7;
                    //XLDocumentHeader.ZamTyp = 14343;
                    //XLDocumentHeader.ZwrNumer = ids;
                    XLDocumentHeader.ZamTyp = 14343;


                    //  XLDocumentHeader.ZamNumer = zlecgid;






                    InfoDocumentHeader = cdn_api.cdn_api.XLNowyDokument(SessionID, ref DocumentHeaderID, XLDocumentHeader);

                    if (InfoDocumentHeader > 0)
                    {
                        error = OpisBłęduAPI(20220, 1, InfoDocumentHeader);
                        _dataload.UpdateApiFromPLC(idframe, error);
                    }





                    cdn_api.XLDokumentElemInfo_20220 XLAddItem = new cdn_api.XLDokumentElemInfo_20220();

                    XLAddItem.ZamTyp = 14346;
                    //    XLAddItem.ZamTyp = 1;
                    XLAddItem.ZamNumer = _dataload.pcaczynnosci(ids, twrgid);



                    //   XLAddItem.ZamNumer = 111111;
                    XLAddItem.Wersja = APIVersion;

                    XLAddItem.TowarKod = twrkod;
                    Entities1 db = new Entities1();

                    // string ilo = 
                    XLAddItem.Ilosc = _dataload.IloscNaPalecie(twrgid).ToString(); ;
                    string SSCC = "";

                   

                    
                    //d    SSCC = db.ZZSPE_SSSC_GEN(1).ToString();
                    string Cecha = "";
                   
                    if (zlecline!=4)
                    {
                        Cecha = db.ZZSPE_CECHA_GEN(dzien, miesiac).FirstOrDefault();
                       
                    }
                    else
                    {
                        SSCC = db.Ety_Sur(1).FirstOrDefault();
                        
                        Cecha = db.ZZSPE_CECHA_GEN_DEKIELKI(SSCC).FirstOrDefault();
                        Zwrot.Add(SSCC);
                        Zwrot.Add(Cecha);
                    
                    }






                 
                    XLAddItem.Cecha = Cecha;
                    
                    //           XLAddItem.DstNumer = ;

                    //     XLAddItem.
                    InfoAddItem = cdn_api.cdn_api.XLDodajPozycje(DocumentHeaderID, XLAddItem);
                    Flagi = XLAddItem.Flagi.ToString();
                    XLAddItem.Opis = (Cecha);
                  

                 
                    if (InfoAddItem > 0)
                    {
                        error = OpisBłęduAPI(20220, 2, InfoDocumentHeader);
                        _dataload.UpdateApiFromPLC(idframe, error);
                    }
                    else
                    {
                        if (zlecline != 4)
                        {
                            SSCC = db.test(1).FirstOrDefault();
                            Zwrot.Add(SSCC);// SSCC POLSKIE
                            Zwrot.Add(Cecha);
                            Zwrot.Add(XLAddItem.DstNumer.ToString());
                        }
                        else
                        {
                      
                            Zwrot.Add(XLAddItem.DstNumer.ToString());
                        }



                       
                    }






                    cdn_api.XLZamkniecieDokumentuInfo_20220 XLClose = new cdn_api.XLZamkniecieDokumentuInfo_20220();

                    XLClose.Wersja = APIVersion;

                    if (zlecline == 4)
                    {
                        XLClose.Tryb = 0;
                    }
                    else { 
               XLClose.Tryb = 1;
                    }


                    InfoXLClose = cdn_api.cdn_api.XLZamknijDokument(DocumentHeaderID, XLClose);
                
                Zwrot.Add(XLClose.GidNumer.ToString());
           
                    if (InfoXLClose > 0)
                {
                    error = OpisBłęduAPI(20220, 7, InfoDocumentHeader);
                    _dataload.UpdateApiFromPLC(idframe, error);
                }

                else
                {
                    error = "OK";
                    _dataload.UpdateApiFromPLC(idframe, error);
                }

                    if(zlecline==4)
                    {
                        _dataload.UpdateDostawy(XLAddItem.DstNumer, SSCC, _dataload.IloscNaPalecie(twrgid));
                    }
                    else
                    {
                        _dataload.UpdateDostawy(XLAddItem.DstNumer, SSCC, _dataload.IloscNaPalecie(twrgid));
                    }

                
                int ss = _dataload.idczynnosciN(zlecgid);

                    bool todo = _dataload.torealization(zlecgid);
                    if (todo==true)
                    {

                  

                        
                cdn_api.XLProdRealizacjaInfo_20220 xLProdRealizacjastart = new cdn_api.XLProdRealizacjaInfo_20220();
                xLProdRealizacjastart.Akcja = 1;
                xLProdRealizacjastart.OperacjaId = ss;
                xLProdRealizacjastart.SposobRealizacji = 2;
                xLProdRealizacjastart.Wersja = 20220;
                xLProdRealizacjastart.Tryb = 2;



                InfoAddItem = cdn_api.cdn_api.XLDodajRealizacjeOperacjiProd(SessionID, xLProdRealizacjastart);
                    }
                  
             
                    
                    //   _dataload.idczynnosci()




                }
                catch (Exception)
   {

   }
   /*********************************Close document***********************************/

            finally
            {
          


                if (cdn_api.cdn_api.XLLogout(SessionID) != 0)
                {
                    //  Console.WriteLine("Błąd przy wylogowaniu.");

                }
                else
                {
                    //     Console.WriteLine("Zakończono i wylogowano");
                }

            }
                starting = false;
               
             
            }
            return Zwrot;
        }
        public List<string> ProdrealizationS(int idoperacji, string twrkod, int dzien, int miesiac, int twrgid, int zlecgid, int idframe)
        {
            starting = true;
            List<string> Zwrot = new List<string>();
            while (starting == true)
            {



                Int32 APIVersion = 20220;
                Int32 SessionID = -1;
                Int32 DocumentTypePM = 1603;
                Int32 DocumentHeaderID = 1;
                int nrgid = 0;

                string Flagi;

                string seria = "PROD";
                Int32 InfoDocumentHeader;
                Int32 InfoAddItem;
                Int32 InfoXLClose;

                try
                {
                    dataload _dataload = new dataload();
                    /**********************************Login**********************************/
                    cdn_api.XLLoginInfo_20220 XLLogin = new cdn_api.XLLoginInfo_20220();
                    XLLogin.Wersja = APIVersion;
                    XLLogin.ProgramID = "API_ODB";
                    XLLogin.Winieta = -1;
                    XLLogin.OpeIdent = Properties.Settings.Default.USER_XL;
                    XLLogin.OpeHaslo = Properties.Settings.Default.PASS_XL;
                    XLLogin.SerwerKlucza = Properties.Settings.Default.KLUCZ_XL;
                    XLLogin.TrybNaprawy = 1;
                    XLLogin.UtworzWlasnaSesje = 1;
                    XLLogin.TrybWsadowy = 0;
                    XLLogin.Baza = Properties.Settings.Default.BAZA_XL;
                    cdn_api.cdn_api.XLLogin(XLLogin, ref SessionID);

                    /********************************Existing connection*********************************/
                    cdn_api.XLPolaczenieInfo_20220 XLConnection = new cdn_api.XLPolaczenieInfo_20220();
                    XLConnection.Wersja = APIVersion;
                    Int32 Result = cdn_api.cdn_api.XLPolaczenie(XLConnection);
                    String conn = XLConnection.ConnectString;

                    bool relopen = _dataload.Open(zlecgid);
                    if (relopen == false)
                    {

                        int newid = _dataload.idczynnosciN(zlecgid);

                        bool todonew = _dataload.torealization(zlecgid);
                        if (todonew == true)
                        {


                            cdn_api.XLProdRealizacjaInfo_20220 xLProdRealizacjastart = new cdn_api.XLProdRealizacjaInfo_20220();
                            xLProdRealizacjastart.Akcja = 1;
                            xLProdRealizacjastart.OperacjaId = newid;
                            xLProdRealizacjastart.SposobRealizacji = 2;
                            xLProdRealizacjastart.Wersja = 20220;
                            xLProdRealizacjastart.Tryb = 2;



                            InfoAddItem = cdn_api.cdn_api.XLDodajRealizacjeOperacjiProd(SessionID, xLProdRealizacjastart);
                            idoperacji = _dataload.idczynnosci(zlecgid);
                        }



                    }


                    int idnew = _dataload.newpczid(idoperacji);
                    cdn_api.XLProdRealizacjaInfo_20220 xLProdRealizacja1 = new cdn_api.XLProdRealizacjaInfo_20220();
                    xLProdRealizacja1.Akcja = 2;
                    xLProdRealizacja1.OperacjaId = idnew;
                    xLProdRealizacja1.SposobRealizacji = 1;
                    xLProdRealizacja1.Wersja = 20220;
                    xLProdRealizacja1.Tryb = 2;
                    InfoAddItem = cdn_api.cdn_api.XLDodajRealizacjeOperacjiProd(SessionID, xLProdRealizacja1);

                    if (InfoAddItem > 0)
                    {
                        break;
                    }

                    int ids = _dataload.newpczid(idoperacji);

                    cdn_api.XLDokumentNagInfo_20220 XLDocumentHeader = new cdn_api.XLDokumentNagInfo_20220();
                    XLDocumentHeader.Wersja = 20220;
                    XLDocumentHeader.Typ = 1617;

                    XLDocumentHeader.Akronim = "PURE ICE";
                    //        XLDocumentHeader.MagazynZ = "";
                    XLDocumentHeader.MagazynD = "SUR";
                    XLDocumentHeader.Seria = "PROD";

                    // XLDocumentHeader.ZamNumer = 7;
                    //XLDocumentHeader.ZamTyp = 14343;
                    //XLDocumentHeader.ZwrNumer = ids;
                    XLDocumentHeader.ZamTyp = 14343;


                    //  XLDocumentHeader.ZamNumer = zlecgid;






                    InfoDocumentHeader = cdn_api.cdn_api.XLNowyDokument(SessionID, ref DocumentHeaderID, XLDocumentHeader);

                    if (InfoDocumentHeader > 0)
                    {
                        error = OpisBłęduAPI(20220, 1, InfoDocumentHeader);
                        _dataload.UpdateApiFromPLC(idframe, error);
                    }





                    cdn_api.XLDokumentElemInfo_20220 XLAddItem = new cdn_api.XLDokumentElemInfo_20220();

                    XLAddItem.ZamTyp = 14346;
                    //    XLAddItem.ZamTyp = 1;
                    XLAddItem.ZamNumer = _dataload.pcaczynnosci(ids, twrgid);
                    //   XLAddItem.ZamNumer = 111111;
                    XLAddItem.Wersja = APIVersion;

                    XLAddItem.TowarKod = twrkod;
                    Entities1 db = new Entities1();

                    // string ilo = 
                    XLAddItem.Ilosc = _dataload.IloscNaPalecie(twrgid).ToString(); ;
                    string SSCC = "";

                    if (twrgid == 5)
                    {




                        SSCC = db.test(4).FirstOrDefault();
                        Zwrot.Add(SSCC);/// SSCC DLA NIEMCA
                    }
                    else
                    {
                        SSCC = db.test(1).FirstOrDefault();
                        Zwrot.Add(SSCC);// SSCC POLSKIE
                    }
                    //d    SSCC = db.ZZSPE_SSSC_GEN(1).ToString();
                    string Cecha = db.ZZSPE_CECHA_GEN(dzien, miesiac).FirstOrDefault();
                    Zwrot.Add(Cecha);


                    XLAddItem.Cecha = Cecha;

                    //           XLAddItem.DstNumer = ;

                    //     XLAddItem.
                    InfoAddItem = cdn_api.cdn_api.XLDodajPozycje(DocumentHeaderID, XLAddItem);
                    Flagi = XLAddItem.Flagi.ToString();
                    XLAddItem.Opis = (Cecha);
                    Zwrot.Add(XLAddItem.DstNumer.ToString());


                    if (InfoAddItem > 0)
                    {
                        error = OpisBłęduAPI(20220, 2, InfoDocumentHeader);
                        _dataload.UpdateApiFromPLC(idframe, error);
                    }








                    cdn_api.XLZamkniecieDokumentuInfo_20220 XLClose = new cdn_api.XLZamkniecieDokumentuInfo_20220();

                    XLClose.Wersja = APIVersion;



                    XLClose.Tryb = 1;



                    InfoXLClose = cdn_api.cdn_api.XLZamknijDokument(DocumentHeaderID, XLClose);

                    Zwrot.Add(XLClose.GidNumer.ToString());
                    if (InfoXLClose > 0)
                    {
                        error = OpisBłęduAPI(20220, 7, InfoDocumentHeader);
                        _dataload.UpdateApiFromPLC(idframe, error);
                    }

                    else
                    {
                        error = "OK";
                        _dataload.UpdateApiFromPLC(idframe, error);
                    }



                    _dataload.UpdateDostawy(XLAddItem.DstNumer, SSCC, _dataload.IloscNaPalecie(twrgid));
                    int ss = _dataload.idczynnosciN(zlecgid);

                    bool todo = _dataload.torealization(zlecgid);
                    if (todo == true)
                    {


                        cdn_api.XLProdRealizacjaInfo_20220 xLProdRealizacjastart = new cdn_api.XLProdRealizacjaInfo_20220();
                        xLProdRealizacjastart.Akcja = 1;
                        xLProdRealizacjastart.OperacjaId = ss;
                        xLProdRealizacjastart.SposobRealizacji = 2;
                        xLProdRealizacjastart.Wersja = 20220;
                        xLProdRealizacjastart.Tryb = 2;



                        InfoAddItem = cdn_api.cdn_api.XLDodajRealizacjeOperacjiProd(SessionID, xLProdRealizacjastart);
                    }

                    //   _dataload.idczynnosci()




                }
                catch (Exception)
                {

                }
                /*********************************Close document***********************************/

                finally
                {



                    if (cdn_api.cdn_api.XLLogout(SessionID) != 0)
                    {
                        //  Console.WriteLine("Błąd przy wylogowaniu.");

                    }
                    else
                    {
                        //     Console.WriteLine("Zakończono i wylogowano");
                    }

                }
                starting = false;


            }
            return Zwrot;
        }


        public List<string> ProdrealizationSUR(int idoperacji, string twrkod, int dzien, int miesiac, int twrgid, int zlecgid, int idframe)
        {
            starting = true;
            List<string> Zwrot = new List<string>();
            while (starting == true)
            {



                Int32 APIVersion = 20220;
                Int32 SessionID = -1;
                Int32 DocumentTypePM = 1603;
                Int32 DocumentHeaderID = 1;
                int nrgid = 0;

                string Flagi;

                string seria = "PROD";
                Int32 InfoDocumentHeader;
                Int32 InfoAddItem;
                Int32 InfoXLClose;

                try
                {
                    dataload _dataload = new dataload();
                    /**********************************Login**********************************/
                    cdn_api.XLLoginInfo_20220 XLLogin = new cdn_api.XLLoginInfo_20220();
                    XLLogin.Wersja = APIVersion;
                    XLLogin.ProgramID = "API_ODB";
                    XLLogin.Winieta = -1;
                    XLLogin.OpeIdent = Properties.Settings.Default.USER_XL;
                    XLLogin.OpeHaslo = Properties.Settings.Default.PASS_XL;
                    XLLogin.SerwerKlucza = Properties.Settings.Default.KLUCZ_XL;
                    XLLogin.TrybNaprawy = 1;
                    XLLogin.UtworzWlasnaSesje = 1;
                    XLLogin.TrybWsadowy = 0;
                    XLLogin.Baza = Properties.Settings.Default.BAZA_XL;
                    cdn_api.cdn_api.XLLogin(XLLogin, ref SessionID);

                    /********************************Existing connection*********************************/
                    cdn_api.XLPolaczenieInfo_20220 XLConnection = new cdn_api.XLPolaczenieInfo_20220();
                    XLConnection.Wersja = APIVersion;
                    Int32 Result = cdn_api.cdn_api.XLPolaczenie(XLConnection);
                    String conn = XLConnection.ConnectString;

                    bool relopen = _dataload.Open(zlecgid);
                    if (relopen == false)
                    {

                        int newid = _dataload.idczynnosciN(zlecgid);

                        bool todonew = _dataload.torealization(zlecgid);
                        if (todonew == true)
                        {


                            cdn_api.XLProdRealizacjaInfo_20220 xLProdRealizacjastart = new cdn_api.XLProdRealizacjaInfo_20220();
                            xLProdRealizacjastart.Akcja = 1;
                            xLProdRealizacjastart.OperacjaId = newid;
                            xLProdRealizacjastart.SposobRealizacji = 2;
                            xLProdRealizacjastart.Wersja = 20220;
                            xLProdRealizacjastart.Tryb = 2;



                            InfoAddItem = cdn_api.cdn_api.XLDodajRealizacjeOperacjiProd(SessionID, xLProdRealizacjastart);
                        }



                    }


                    int idnew = _dataload.newpczid(idoperacji);
                    cdn_api.XLProdRealizacjaInfo_20220 xLProdRealizacja1 = new cdn_api.XLProdRealizacjaInfo_20220();
                    xLProdRealizacja1.Akcja = 2;
                    xLProdRealizacja1.OperacjaId = idnew;
                    xLProdRealizacja1.SposobRealizacji = 1;
                    xLProdRealizacja1.Wersja = 20220;
                    xLProdRealizacja1.Tryb = 2;
                    InfoAddItem = cdn_api.cdn_api.XLDodajRealizacjeOperacjiProd(SessionID, xLProdRealizacja1);

                    if (InfoAddItem > 0)
                    {
                        break;
                    }

                    int ids = _dataload.newpczid(idoperacji);

                    cdn_api.XLDokumentNagInfo_20220 XLDocumentHeader = new cdn_api.XLDokumentNagInfo_20220();
                    XLDocumentHeader.Wersja = 20220;
                    XLDocumentHeader.Typ = 1617;

                    XLDocumentHeader.Akronim = "PURE ICE";
                    //        XLDocumentHeader.MagazynZ = "";
                    XLDocumentHeader.MagazynD = "SUR";
                    XLDocumentHeader.Seria = "PROD";

                    // XLDocumentHeader.ZamNumer = 7;
                    //XLDocumentHeader.ZamTyp = 14343;
                    //XLDocumentHeader.ZwrNumer = ids;
                    XLDocumentHeader.ZamTyp = 14343;


                    //  XLDocumentHeader.ZamNumer = zlecgid;






                    InfoDocumentHeader = cdn_api.cdn_api.XLNowyDokument(SessionID, ref DocumentHeaderID, XLDocumentHeader);

                    if (InfoDocumentHeader > 0)
                    {
                        error = OpisBłęduAPI(20220, 1, InfoDocumentHeader);
                        _dataload.UpdateApiFromPLC(idframe, error);
                    }





                    cdn_api.XLDokumentElemInfo_20220 XLAddItem = new cdn_api.XLDokumentElemInfo_20220();
                    string SSCC = "";

                    XLAddItem.ZamTyp = 14346;
                    //    XLAddItem.ZamTyp = 1;
                    XLAddItem.ZamNumer = _dataload.pcaczynnosci(ids, twrgid);
                    //   XLAddItem.ZamNumer = 111111;
                    XLAddItem.Wersja = APIVersion;

                    XLAddItem.TowarKod = twrkod;
                    Entities1 db = new Entities1();

                    // string ilo = 
                    XLAddItem.Ilosc = _dataload.IloscNaPalecie(twrgid).ToString(); ;
               
                    string Cecha = db.ZZSPE_CECHA_GEN(dzien, miesiac).FirstOrDefault();


                    XLAddItem.Cecha = Cecha;

                    //           XLAddItem.DstNumer = ;

                    //     XLAddItem.
                    InfoAddItem = cdn_api.cdn_api.XLDodajPozycje(DocumentHeaderID, XLAddItem);
                    Flagi = XLAddItem.Flagi.ToString();
                    XLAddItem.Opis = (Cecha);
                 


                    if (InfoAddItem > 0)
                    {
                        error = OpisBłęduAPI(20220, 2, InfoDocumentHeader);
                        _dataload.UpdateApiFromPLC(idframe, error);
                    }
                    else
                    {
                        
                        if (twrgid == 5)
                        {




                            SSCC = db.test(4).FirstOrDefault();
                            Zwrot.Add(SSCC);/// SSCC DLA NIEMCA
                        }
                        else
                        {
                            SSCC = db.test(1).FirstOrDefault();
                            Zwrot.Add(SSCC);// SSCC POLSKIE
                            Zwrot.Add(Cecha);
                            Zwrot.Add(XLAddItem.DstNumer.ToString());
                        }
                        //d    SSCC = db.ZZSPE_SSSC_GEN(1).ToString();
                    }








                    cdn_api.XLZamkniecieDokumentuInfo_20220 XLClose = new cdn_api.XLZamkniecieDokumentuInfo_20220();

                    XLClose.Wersja = APIVersion;



                    XLClose.Tryb = 1;



                    InfoXLClose = cdn_api.cdn_api.XLZamknijDokument(DocumentHeaderID, XLClose);

                   
                    if (InfoXLClose > 0)
                    {
                        int gidd =0;
                        Zwrot.Add(XLClose.GidNumer.ToString());
                        error = OpisBłęduAPI(20220, 7, InfoDocumentHeader);
                        _dataload.UpdateApiFromPLC(idframe, error);
                    }

                    else
                    {
                        Zwrot.Add(XLClose.GidNumer.ToString());
                        error = "OK";
                        _dataload.UpdateApiFromPLC(idframe, error);
                    }



                    _dataload.UpdateDostawy(XLAddItem.DstNumer, SSCC, _dataload.IloscNaPalecie(twrgid));
                    int ss = _dataload.idczynnosciN(zlecgid);

                    bool todo = _dataload.torealization(zlecgid);
                    if (todo == true)
                    {


                        cdn_api.XLProdRealizacjaInfo_20220 xLProdRealizacjastart = new cdn_api.XLProdRealizacjaInfo_20220();
                        xLProdRealizacjastart.Akcja = 1;
                        xLProdRealizacjastart.OperacjaId = ss;
                        xLProdRealizacjastart.SposobRealizacji = 2;
                        xLProdRealizacjastart.Wersja = 20220;
                        xLProdRealizacjastart.Tryb = 2;



                        InfoAddItem = cdn_api.cdn_api.XLDodajRealizacjeOperacjiProd(SessionID, xLProdRealizacjastart);
                    }

                    //   _dataload.idczynnosci()




                }
                catch (Exception)
                {

                }
                /*********************************Close document***********************************/

                finally
                {



                    if (cdn_api.cdn_api.XLLogout(SessionID) != 0)
                    {
                        //  Console.WriteLine("Błąd przy wylogowaniu.");

                    }
                    else
                    {
                        //     Console.WriteLine("Zakończono i wylogowano");
                    }

                }
                starting = false;


            }
            return Zwrot;
        }

        public List<string> Testowa ()
        {
            

            Int32 APIVersion = 20220;
            Int32 SessionID = -1;
            Int32 DocumentTypePM = 1603;
            Int32 DocumentHeaderID = 1;
            int nrgid = 0;
            List<string> aa = new List<string>();
            try
            {

                string Flagi;

                string seria = "PROD";
                Int32 InfoDocumentHeader;
                Int32 InfoAddItem;
                Int32 InfoXLClose;

                /**********************************Login**********************************/
                cdn_api.XLLoginInfo_20220 XLLogin = new cdn_api.XLLoginInfo_20220();
                XLLogin.Wersja = APIVersion;
                XLLogin.ProgramID = "API_ODB";
                XLLogin.Winieta = -1;
                XLLogin.OpeIdent = "admin";
                XLLogin.OpeHaslo = "admPure_";
                XLLogin.TrybNaprawy = 1;
                XLLogin.UtworzWlasnaSesje = 1;
                XLLogin.TrybWsadowy = 0;
                XLLogin.Baza = "PURE-ICE 2021";
                cdn_api.cdn_api.XLLogin(XLLogin, ref SessionID);

                /********************************Existing connection*********************************/
                cdn_api.XLPolaczenieInfo_20220 XLConnection = new cdn_api.XLPolaczenieInfo_20220();
                XLConnection.Wersja = APIVersion;
                Int32 Result = cdn_api.cdn_api.XLPolaczenie(XLConnection);
                String conn = XLConnection.ConnectString;
                cdn_api.XLProdObiektyDoDokumentowInfo_20220 zz = new cdn_api.XLProdObiektyDoDokumentowInfo_20220();
                zz.GidTyp = 14343;
                zz.GidNumer = 193;
                zz.Wersja = APIVersion;
            int test=    cdn_api.cdn_api.XLProdObiektyDoDokumentow(zz);



             
             //   cdn_api.XLDokumentNagInfo_20220 XLDocumentHeader = new cdn_api.XLDokumentNagInfo_20220();
          
                cdn_api.XLProdGenerujDokumentInfo_20220 aasdsds = new cdn_api.XLProdGenerujDokumentInfo_20220();
                aasdsds.Wersja = 20220;
                aasdsds.TypDokumentu = 1617;
                aasdsds.TypObiektu = 107971;

                int aaadsadsdsaa = cdn_api.cdn_api.XLProdGenerujDokumentyDlaObiektow(SessionID,aasdsds);
            }



            catch (Exception)
            {

            }

            /*********************************Close document***********************************/

            finally
            {


                if (cdn_api.cdn_api.XLLogout(SessionID) != 0)
                {
                    //  Console.WriteLine("Błąd przy wylogowaniu.");

                }
                else
                {
                    //     Console.WriteLine("Zakończono i wylogowano");
                }

            }
            return aa;
        }


        public int RWCLOSE1(int gidnumer)
        {
            Int32 APIVersion = 20220;
            Int32 SessionID = -1;
            Int32 DocumentTypePM = 1603;
            Int32 DocumentHeaderID = 1;
            int nrgid = 0;
            int close = 1;
            string Flagi;
            int apierror = 0;
            string error;
            string seria = "PROD";
            Int32 InfoDocumentHeader;
            Int32 InfoAddItem;
            Int32 InfoXLClose;
            List<string> Zwrot = new List<string>();
            try
            {
                dataload _dataload = new dataload();
                /**********************************Login**********************************/
                cdn_api.XLLoginInfo_20220 XLLogin = new cdn_api.XLLoginInfo_20220();
                XLLogin.Wersja = APIVersion;
                XLLogin.ProgramID = "RW_APICLOSE";
                XLLogin.Winieta = -1;
                XLLogin.OpeIdent = "XLWORK";
                XLLogin.OpeHaslo = "xlwork";
                XLLogin.SerwerKlucza = Properties.Settings.Default.KLUCZ_XL;
                XLLogin.TrybNaprawy = 1;
                XLLogin.UtworzWlasnaSesje = 1;
                XLLogin.TrybWsadowy = 0;
                XLLogin.Baza = Properties.Settings.Default.BAZA_XL;
                cdn_api.cdn_api.XLLogin(XLLogin, ref SessionID);





                /********************************Existing connection*********************************/
                cdn_api.XLPolaczenieInfo_20220 XLConnection = new cdn_api.XLPolaczenieInfo_20220();
                XLConnection.Wersja = APIVersion;
                Int32 Result = cdn_api.cdn_api.XLPolaczenie(XLConnection);
                String conn = XLConnection.ConnectString;


                cdn_api.XLOtwarcieNagInfo_20220 XLDocumentHeader = new cdn_api.XLOtwarcieNagInfo_20220();
                XLDocumentHeader.GIDNumer = gidnumer;
                XLDocumentHeader.Wersja = APIVersion;
                XLDocumentHeader.GIDTyp = 1616;
                XLDocumentHeader.Tryb = 2;







                InfoDocumentHeader = cdn_api.cdn_api.XLOtworzDokument(SessionID, ref DocumentHeaderID, XLDocumentHeader);


                if (InfoDocumentHeader > 0)
                {
                    error = OpisBłęduAPI(20220, 37, InfoDocumentHeader);
         
                }




                cdn_api.XLZamkniecieDokumentuInfo_20220 XLClose = new cdn_api.XLZamkniecieDokumentuInfo_20220();
                XLClose.Wersja = APIVersion;
                XLClose.GidNumer = gidnumer;

                XLClose.Tryb = 0;









                InfoXLClose = cdn_api.cdn_api.XLZamknijDokument(DocumentHeaderID, XLClose);
                close = InfoXLClose;
                if (InfoXLClose > 0)
                {


                    error = OpisBłęduAPI(20220, 7, InfoDocumentHeader);
               
                }





            }
            catch (Exception)
            {

            }
            /*********************************Close document***********************************/

            finally
            {



                if (cdn_api.cdn_api.XLLogout(SessionID) != 0)
                {
                    //  Console.WriteLine("Błąd przy wylogowaniu.");

                }
                else
                {
                    //     Console.WriteLine("Zakończono i wylogowano");
                }

            }

            return close;
        }


    }


}

