//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PureIce_Production_v2
{
    using System;
    using System.Collections.Generic;
    
    public partial class AllowedLogisticUnits
    {
        public int Id { get; set; }
        public int LogisticUnitTypeId { get; set; }
        public int ParametersId { get; set; }
    
        public virtual LogisticUnitTypes LogisticUnitTypes { get; set; }
        public virtual Parameters Parameters { get; set; }
    }
}
