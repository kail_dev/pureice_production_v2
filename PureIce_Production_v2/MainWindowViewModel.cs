﻿using NLog;
using PureIce_Production_v2.PLC;
using PureIce_Production_v2.SQL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace PureIce_Production_v2
{
  public  class MainWindowViewModel : INotifyPropertyChanged
    {
        private BackgroundWorker SS;
        private BackgroundWorker sender1;
        private BackgroundWorker plcbg;
        private BackgroundWorker bw1;
        private static Thread t1;
        private static Thread t2;
        private volatile bool m_StopThread;
        private ObservableCollection<ZZSPE_FROM_PLC> plc;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ObservableCollection<ZZSPE_FROM_PLC> Plc
        {
            get => plc; set
            {
                plc = value;
                OnPropertyChanged();
         //    PropertyChanged(this,new PropertyChangedEventArgs("Plc"));
            }
        }


   
        public  void Listen()
        {

          
            

           
        }

        public void Creator()
        {
            SS = new BackgroundWorker();
            SS.DoWork += new DoWorkEventHandler(Creator_DoWork);
            SS.WorkerSupportsCancellation = true;

            SS.RunWorkerAsync();



            logger.Info("start");
            //    sender.CancelAsync();


        }

        void Creator_DoWork(object sender, DoWorkEventArgs e)

        {

            var bw = (BackgroundWorker)SS;
            while (!bw.CancellationPending)
            {


                dataload _dataload = new dataload();
                var tosend = _dataload.FrameToWork();
                if (tosend.Count >0)
                {

                    Creator cr = new Creator();
                    cr.creating();





                }
            }
            if (bw.CancellationPending)
            {
                e.Cancel = true;
                //   MessageBox.Show("koniec");

            }
            else
            {
                bw.ReportProgress(100);
            }





        }

        public void Sender( )
        {
           sender1 = new BackgroundWorker();
            sender1.DoWork += new DoWorkEventHandler(sender_DoWork);
            sender1.WorkerSupportsCancellation = true;

           sender1.RunWorkerAsync();

          

            logger.Info("start");
       //    sender.CancelAsync();
            

        }

        public void CancleSend()
        {
           //plcbg.CancelAsync();
            sender1.CancelAsync();

        }
        public void CancleDOK()
        {
            //plcbg.CancelAsync();
            SS.CancelAsync();

        }



        void sender_DoWork(object sender, DoWorkEventArgs e)

        {
            
            var bw = (BackgroundWorker)sender1;
            while (!bw.CancellationPending)
            { 
              

                    dataload _dataload = new dataload();
                    bool tosend = _dataload.tosend();
                    if (tosend == true)
                    {

                        SendToPLC dal = new SendToPLC();
                        dal.Send();



                

                }
            }
            if (bw.CancellationPending)
            {
                e.Cancel = true;
             //   MessageBox.Show("koniec");

            }
            else
            {
                bw.ReportProgress(100);
            }



           

        }



        public void Listener()
        {
            plcbg = new BackgroundWorker();
            plcbg.WorkerSupportsCancellation = true;
            plcbg.DoWork += new DoWorkEventHandler(plcbg_DoWork);
            plcbg.RunWorkerAsync();
          


                                 


        }


       
        void plcbg_DoWork(object Listener, DoWorkEventArgs e)

        {
            var bw = (BackgroundWorker)plcbg;
            while (!bw.CancellationPending)
            {
                CommunicationPLC plc = new CommunicationPLC();

                byte[] buffer =CommunicationPLC.PlcConnect(Properties.Settings.Default.PLC_IP, Properties.Settings.Default.PORT_UDP_RECIVER);




            }
        
            if (bw.CancellationPending)
            {
                e.Cancel = true;
              //  bw.ReportProgress(100);
                MessageBox.Show("koniec");

            }
            else
            {
                bw.ReportProgress(100);
            }





        }
        public void StopListen()
        {
            bw1.CancelAsync();
            //sender1.CancelAsync();
            //  t1.Join();
            //    t1.Abort();
            //System.Diagnostics.Process.GetProcessesByName("PLC");
        }

        public void RefreshDataTable()
        {
            dataload data = new dataload();
            
            Plc = new ObservableCollection<ZZSPE_FROM_PLC>(data.List_From_PLC());
        }

        public void RefreshDataTable_ALL()
        {
            dataload data = new dataload();

            Plc = new ObservableCollection<ZZSPE_FROM_PLC>(data.List_From_PLC_ALL());
        }



        public event PropertyChangedEventHandler PropertyChanged;
       protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
      {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
      }
    }
}
