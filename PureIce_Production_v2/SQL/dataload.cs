﻿using PureIce_Production_v2.PLC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PureIce_Production_v2.SQL
{
  public  class dataload
    {
        public virtual List<ZZSPE_FROM_PLC> List_From_PLC(string code = "")
        {
            var ent = new Entities1();
            var dane = ent.ZZSPE_FROM_PLC.ToList();
             List<ZZSPE_FROM_PLC> obj = dane.OrderByDescending(k => k.ID).ToList();
            List<ZZSPE_FROM_PLC> objj = obj.Take(100).ToList();
            return objj;
        }
        public virtual List<ZZSPE_FROM_PLC> List_From_PLC_ALL(string code = "")
        {
            var ent = new Entities1();
            var dane = ent.ZZSPE_FROM_PLC.ToList();
            List<ZZSPE_FROM_PLC> obj = dane.OrderByDescending(k => k.ID).ToList();
   
            return obj;
        }



        public virtual List<ZZSPE_FROM_PLC> FrameToWork(string code = "")
        {
            var ent = new Entities1();
            var dane = ent.ZZSPE_FROM_PLC.Where(e => e.STAN == 0).ToList();
            return dane;
        }




        public virtual List<ZZSPE_DaneWeHist> ListToReprint(int idfromplc)
        {
            var ent = new Entities1();
            var dane = ent.ZZSPE_DaneWeHist.Where(k=> k.ID_DWH==idfromplc && k.PRINTER_ID==0).ToList();
         
            return dane;
        }

        public virtual bool UpdateDstOpisy(int iddst, string opis)
        {
            Entities1 ent = new Entities1();
    
            var plcRecord = ent.Set<DstOpisy>();
            plcRecord.Add(new DstOpisy() {DsO_DstTyp=160, DsO_DstFirma = 1254150, DsO_DstNumer = iddst, DsO_DstLp = 0, DsO_Typ = 0, DsO_Opis = opis });
            return ent.SaveChanges() > 0;


        }
        public virtual bool StanFromPlc(int id,int stan)
        {
            Entities1 ent = new Entities1();
            ZZSPE_FROM_PLC plcRecord = ent.ZZSPE_FROM_PLC.Where(e => e.ID== id).First();
            plcRecord.STAN =1;
           
            //
            return ent.SaveChanges() > 0;


        }


        public virtual bool UpdateDostawy(int id, string ean,decimal ilosc)
        {
            Entities1 ent = new Entities1();
            Dostawy plcRecord = ent.Dostawy.Where(e => e.Dst_GIDNumer == id).First();
            plcRecord.Dst_Ean = ean;
            plcRecord.Dst_Ilosc = ilosc;
           //
           return ent.SaveChanges() > 0;


        }


        public int IdSessionToUpdateCLOSE(string appname = "")
        {
            var ent = new Entities1();
            var dane = ent.Sesje.Where(e => e.SES_Modul == "X:APIPWPROD" && e.SES_Aktywna == 0).Select(e => e.SES_SesjaID).FirstOrDefault();
            //   int wynik= Convert.ToInt32(dane);
            return dane;


        }

        public int IdSessionToUpdate(string appname="")
        {
            var ent = new Entities1();
                  var dane = ent.Sesje.Where(e => e.SES_Modul == "X:API_ODB" && e.SES_Aktywna == 0).Select(e => e.SES_SesjaID).FirstOrDefault();
            //   int wynik= Convert.ToInt32(dane);
            return  dane;


        }
        public int gidrw(int operacja)
        {
            var ent = new Entities1();
            var dane = ent.TraSElem.Where(e => e.TrS_ZlcNumer == operacja && e.TrS_GIDTyp == 1616).Select(k => k.TrS_GIDNumer).FirstOrDefault();
            //   int wynik= Convert.ToInt32(dane);
            return dane;


        }



        public int IDFROMPLC(int progmsg, int zlecgid)
        {
            var ent = new Entities1();
            var dane = ent.ZZSPE_FROM_PLC.Where(e => e.PROG_MSG == progmsg && e.ZLEC_GID == zlecgid).Select(e => e.ID).FirstOrDefault();
            //   int wynik= Convert.ToInt32(dane);
            return dane;


        }


        public int IDFROMDWH(int nrpalety, int zlecgid)
        {
            var ent = new Entities1();
            var dane = ent.ZZSPE_DaneWeHist.Where(e => e.NR_PALETY.Value == nrpalety && e.ZLEC_GID.Value == zlecgid && e.PRINTER_ID.Value==0).Select(e => e.ID_DWH).FirstOrDefault();
            //   int wynik= Convert.ToInt32(dane);
           
            
            return dane;


        }

        public virtual bool UpdateFromPLC(int zlecgid,int palnr,string nazwadok)
        {


            var ent = new Entities1();
            ZZSPE_FROM_PLC plcRecord = ent.ZZSPE_FROM_PLC.Where(e => e.ZLEC_GID == zlecgid && e.NR_PALETY==palnr).First();
            plcRecord.PW_DOKUMENT = nazwadok;


            return ent.SaveChanges() > 0;


        }


        public virtual bool UpdateFromPLCPRINT(int id)
        {


            var ent = new Entities1();
            ZZSPE_FROM_PLC plcRecord = ent.ZZSPE_FROM_PLC.Where(e => e.ID == id).First();
            plcRecord.PW_DOKUMENT = "Reprint";


            return ent.SaveChanges() > 0;


        }

        public virtual bool UpdateApiFromPLC(int id, string status)
        {


            var ent = new Entities1();
            ZZSPE_FROM_PLC plcRecord = ent.ZZSPE_FROM_PLC.Where(e => e.ID == id).First();
            plcRecord.API_STATUS = status;


            return ent.SaveChanges() > 0;


        }



        public virtual bool UpdateSession(int id)
        {


            var ent = new Entities1();
            Sesje plcRecord = ent.Sesje.Where(e => e.SES_SesjaID == id).First();
            plcRecord.SES_Aktywna = 1;


            return ent.SaveChanges() > 0;


        }

        public virtual bool SaveTelegram(ZZSPE_FROM_PLC e)
        {
            Entities1 ent = new Entities1();
            var plcRecord = ent.Set<ZZSPE_FROM_PLC>();

            plcRecord.Add(new ZZSPE_FROM_PLC() { MSG_ID = e.MSG_ID, PROG_MSG = e.PROG_MSG, ROK = e.ROK, DZIEN = e.DZIEN, MIESIAC = e.MIESIAC, GODZINA = e.GODZINA, MINUTA = e.MINUTA, SEKUNDA = e.SEKUNDA, ZLEC_GID = e.ZLEC_GID, ZLEC_LINE = e.ZLEC_LINE, TWR_GID = e.TWR_GID, ILOSC = 1, STAN = 0, ZMIANA = e.ZMIANA, PRINTER_ID = e.PRINTER_ID, NR_PALETY = e.NR_PALETY, DATA_DODANIA=DateTime.Now });


            return ent.SaveChanges() > 0;
            
        }







        public virtual bool SaveDWH(ZZSPE_DaneWeHist e)
        {
            Entities1 ent = new Entities1();
            var plcRecord = ent.Set<ZZSPE_DaneWeHist>();

            plcRecord.Add(new ZZSPE_DaneWeHist() { Pozycja = e.Pozycja, ID=e.ID,MSG_ID = e.MSG_ID, PROG_MSG = e.PROG_MSG, ROK = e.ROK, DZIEN = e.DZIEN, MIESIAC = e.MIESIAC, GODZINA = e.GODZINA, MINUTA = e.MINUTA, SEKUNDA = e.SEKUNDA, ZLEC_GID = e.ZLEC_GID, ZLEC_LINE = e.ZLEC_LINE, TWR_GID = e.TWR_GID, ILOSC = e.ILOSC, STAN = e.STAN, ZMIANA = e.ZMIANA, PRINTER_ID = e.PRINTER_ID, NR_PALETY = e.NR_PALETY, DOK_GIDNumer = e.DOK_GIDNumer, DST_GIDNumer = e.DST_GIDNumer, SSCC = e.SSCC, Cecha = e.Cecha, EAN = e.EAN, EANOPAK = e.EANOPAK, KOD_ZBIORCZY = e.KOD_ZBIORCZY ,DST_GIDTyp=e.DST_GIDTyp,DOK_GIDTyp=e.DOK_GIDTyp});


            return ent.SaveChanges() > 0;
        }

        public bool tosend()
        {
            var ent = new Entities1();
            //   var tosend = ent.ZZSPE_FROM_PLC.ToList();
            var tosend = ent.ZZSPE_TO_PLC.Where(e => e.STAN == 1).Count();
            if (tosend == 0) { return false; }
            else { return true; }
        }

        public List<ZZSPE_TO_PLC> ToPlcSend()
        {
            var ent = new Entities1();
            var listtosend = ent.ZZSPE_TO_PLC.Where(e => e.STAN == 1).ToList();
            return listtosend;
        }
        public virtual bool UpdatePlcTelegram(int id)

        {
          //  var logger = NLog.LogManager.GetCurrentClassLogger();
            Entities1 ent = new Entities1();
            ZZSPE_TO_PLC plcRecord = ent.ZZSPE_TO_PLC.Where(e => e.ID == id).First();
            DateTime dt = DateTime.Now;
            plcRecord.STAN = 2;
   

            return ent.SaveChanges() > 0;
        }


        public virtual bool Print(ZZSPE_PRINT e)
        {
            Entities1 ent = new Entities1();
            var plcRecord = ent.Set<ZZSPE_PRINT>();

            plcRecord.Add(new ZZSPE_PRINT() { DOK_GIDNumer = e.DOK_GIDNumer, DOK_GIDTyp = e.DOK_GIDTyp, DOK_NAZ = e.DOK_NAZ, ID_PRINTER = e.ID_PRINTER, NR_PALETY = e.NR_PALETY, TWR_CECHA = e.TWR_CECHA, TWR_DATA = e.TWR_DATA, TWR_DATA2 = e.TWR_DATA2, TWR_DATA_DE = e.TWR_DATA_DE, TWR_DATA_DE1 = e.TWR_DATA_DE1, TWR_DATA_DE2 = e.TWR_DATA_DE2, TWR_EAN = e.TWR_EAN, TWR_EAN_OPAK = e.TWR_EAN_OPAK, TWR_EAN_PALETY = e.TWR_EAN_PALETY, TWR_ILOSC = e.TWR_ILOSC, TWR_NAZWA = e.TWR_NAZWA, TWR_SSCC = e.TWR_SSCC, TWR_ILOSC_OPAK = e.TWR_ILOSC_OPAK, TWR_ILOSC_SZT = e.TWR_ILOSC_SZT, TWR_KOD_ZBIORCZY = e.TWR_KOD_ZBIORCZY, TWR_NAZWA_DE = e.TWR_NAZWA_DE, TWR_WAGA = e.TWR_WAGA, STATUS = e.STATUS, ZLEC_LINE = e.ZLEC_LINE, ZPL_ID = e.ZPL_ID });


            return ent.SaveChanges() > 0;
        }
        public int idczynnosci(int id)
        {
            var ent = new Entities1();
            var dane = ent.ProdCzynnosci.Where(e => e.PCZ_PZLId == id && e.PCZ_Status== "Uruchomiona").Select(e => e.PCZ_Id).FirstOrDefault();
            //   int wynik= Convert.ToInt32(dane);
            return dane;


        }


        public int idczynnosciN(int id)
        {
            var ent = new Entities1();
            var dane = ent.ProdCzynnosci.Where(e => e.PCZ_PZLId == id && e.PCZ_Status != "Zrealizowana").ToList();
            var dane2 = dane.Where(e=>e.PCZ_Status!="W realizacji").Select(e => e.PCZ_Id).FirstOrDefault();
            //   int wynik= Convert.ToInt32(dane);
            return dane2;


        }
        public int idczynnoscitoclick(int id)
        {
            var ent = new Entities1();
            var dane = ent.ProdCzynnosci.Where(e => e.PCZ_PZLId == id && e.PCZ_Status != "Zrealizowana").ToList();
            var dane1 = dane.Where(e => e.PCZ_Status != "Uruchomiona").Select(e => e.PCZ_Id).FirstOrDefault();
            //   int wynik= Convert.ToInt32(dane);
            return dane1;


        }

        public bool Open(int id)
        {
            var ent = new Entities1();
            var dane = ent.ProdCzynnosci.Where(e => e.PCZ_PZLId == id && e.PCZ_Status == "Uruchomiona").Count();
            if (dane >0)
            {
                return true;

            }
            else
            {
                return false;
            }
            //   int wynik= Convert.ToInt32(dane);
           


        }


        public List<ZZSPE_DaneWeHist> FrameToReprint(int Dzien, int rok, int miesiac, int godzina, int minuta, int sekunda, int zlecgid, int palnr)
        {
            var ent = new Entities1();
            var dane = ent.ZZSPE_DaneWeHist.Where(e => e.ROK == rok && e.MIESIAC==miesiac && e.DZIEN==Dzien && e.GODZINA==godzina && e.MINUTA==minuta&& e.SEKUNDA==sekunda && e.ZLEC_GID==zlecgid && e.NR_PALETY==palnr ).ToList();

            return dane;
           

           
           
           
            }
        //   int wynik= Convert.ToInt32(dane);





        public bool torealization(int zlecgid)
        {
            var ent = new Entities1();
            var dane = ent.ProdCzynnosci.Where(e => e.PCZ_PZLId == zlecgid && e.PCZ_Status != "Zrealizowana"  && e.PCZ_Realizuje==0).ToList();
     //       var dane2 = dane.Where(e => e.PCZ_Status != "W realizacji").ToList();
            if (dane.Count>0)
            { return true; }
            else
            {
                return false;
            }


        }



        public int newpczid(int id)
        {
            var ent = new Entities1();
            var dane = ent.ProdCzynnosci.Where(e => e.PCZ_Realizuje == id ).Select(e => e.PCZ_Id).FirstOrDefault();
            //   int wynik= Convert.ToInt32(dane);
            return dane;


        }


        public List<ZZSPE_DaneWeHist> FRAMERW (string kod)
        {
            var ent = new Entities1();
            var dane = ent.ZZSPE_DaneWeHist.Where(k => k.KOD_ZBIORCZY == kod).ToList();
            return dane;
        }


        public int pcaczynnosci(int id, int twrgid)
        {
            var ent = new Entities1();
            var dane = ent.ProdZasoby.Where(e => e.PZA_Czynnosc == id && e.PZA_TwrNumer==twrgid ).Select(e => e.PZA_Id).FirstOrDefault();
            //   int wynik= Convert.ToInt32(dane);
            return dane;


        }
        public string EanOpak(int twrid)
        {
            var ent = new Entities1();
            var dane = ent.TwrKody.Where(e => e.TwK_TwrNumer == twrid && e.TwK_TypKodu==1 && e.TwK_Jm == "opak").Select(e => e.TwK_Kod).FirstOrDefault();
            //   int wynik= Convert.ToInt32(dane);
            return dane;


        }

        public string EANPALETY(int twrid)
        {
            var ent = new Entities1();
            var dane = ent.TwrKody.Where(e => e.TwK_TwrNumer == twrid && e.TwK_TypKodu == 1 && e.TwK_Jm == "pal").Select(e => e.TwK_Kod).FirstOrDefault();
            //   int wynik= Convert.ToInt32(dane);
            return dane;


        }


        public decimal IloscNaPalecie(int twrid)
        {
            var ent = new Entities1();
            var dane = ent.TwrJm.Where(e => e.TwJ_TwrNumer == twrid && e.TwJ_JmZ == "pal").Select(e => e.TwJ_PrzeliczL).FirstOrDefault();
            //       var dane = ent.TwrKody.Where(e => e.TwK_TwrNumer == twrid && e.TwK_TypKodu == 1 && e.TwK_Jm == "pal").Select(e => e.TWK).FirstOrDefault();
            //   int wynik= Convert.ToInt32(dane);
            return dane.Value;


        }
        public decimal IloscOpak(int twrid)
        {
            var ent = new Entities1();
            var dane = ent.TwrJm.Where(e => e.TwJ_TwrNumer == twrid && e.TwJ_JmZ == "opak").Select(e => e.TwJ_PrzeliczL).FirstOrDefault();
            //       var dane = ent.TwrKody.Where(e => e.TwK_TwrNumer == twrid && e.TwK_TypKodu == 1 && e.TwK_Jm == "pal").Select(e => e.TWK).FirstOrDefault();
            //   int wynik= Convert.ToInt32(dane);
            return dane.Value;


        }


        public string IdToTwrKod(int twrid)
        {
            var ent = new Entities1();
            var dane = ent.TwrKarty.Where(e => e.Twr_GIDNumer == twrid).Select(e => e.Twr_Kod).FirstOrDefault();
            //   int wynik= Convert.ToInt32(dane);
            return dane;


        }

        public string TwrEan(int twrid)
        {
            var ent = new Entities1();
            var dane = ent.TwrKarty.Where(e => e.Twr_GIDNumer == twrid).Select(e => e.Twr_Ean).FirstOrDefault();
            //   int wynik= Convert.ToInt32(dane);
            return dane;


        }

        public string IdToTwrNazwa(int twrid)
        {
            var ent = new Entities1();
            var dane = ent.TwrKarty.Where(e => e.Twr_GIDNumer == twrid).Select(e => e.Twr_Nazwa).FirstOrDefault();
            //   int wynik= Convert.ToInt32(dane);
            return dane;


        }

    }
}
