//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PureIce_Production_v2
{
    using System;
    using System.Collections.Generic;
    
    public partial class ParameterClassesObjectTypes
    {
        public int Id { get; set; }
        public int ParameterClassId { get; set; }
        public short ObjectTypeId { get; set; }
        public Nullable<int> ObjectSubtypeId { get; set; }
    
        public virtual ObjectTypes ObjectTypes { get; set; }
        public virtual ParameterClasses ParameterClasses { get; set; }
    }
}
