//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PureIce_Production_v2
{
    using System;
    using System.Collections.Generic;
    
    public partial class StageProcessTasks
    {
        public int Id { get; set; }
        public int StageId { get; set; }
        public Nullable<int> ProcessTaskId { get; set; }
        public int Order { get; set; }
        public Nullable<int> LoopTaskId { get; set; }
        public bool IsOptional { get; set; }
        public bool IsPosition { get; set; }
        public Nullable<int> ProcessTaskStage { get; set; }
        public Nullable<int> AvailabilityId { get; set; }
    
        public virtual AvailabilitySettings AvailabilitySettings { get; set; }
        public virtual ProcessLoops ProcessLoops { get; set; }
        public virtual ProcessStages ProcessStages { get; set; }
        public virtual ProcessTasks ProcessTasks { get; set; }
    }
}
