//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PureIce_Production_v2
{
    using System;
    using System.Collections.Generic;
    
    public partial class SourceElementsRelations
    {
        public int Id { get; set; }
        public int WmsItemId { get; set; }
        public Nullable<int> SourceWmsItemId { get; set; }
        public int SourceLotElementId { get; set; }
        public int SourceLotId { get; set; }
        public decimal Quantity { get; set; }
        public Nullable<decimal> RealizedQuantity { get; set; }
    
        public virtual Items Items { get; set; }
    }
}
